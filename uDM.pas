unit uDM;

interface

uses
  System.SysUtils, System.Classes, UniProvider, SQLiteUniProvider, Data.DB,
  MemDS, DBAccess, Uni, VirtualQuery;

type
  TDM = class(TDataModule)
    UniConnection: TUniConnection;
    qMasterDBTransfer: TUniQuery;
    SQLiteUniProvider: TSQLiteUniProvider;
    qMonster: TUniQuery;
    MonsterDataSource: TUniDataSource;
    qMonsterid: TIntegerField;
    qMonsterinternal_id: TMemoField;
    qMonsterMonstername: TMemoField;
    qMonsterProperty: TUniQuery;
    qMonsterPropertyid: TIntegerField;
    qMonsterPropertydwID: TMemoField;
    qMonsterPropertyszName: TMemoField;
    qMonsterPropertyAdvanced: TUniQuery;
    qMonsterPropertyAdvancedid: TIntegerField;
    qMonsterPropertyAdvanceddwID: TMemoField;
    qMonsterPropertyAdvancedszName: TMemoField;
    qMonsterPropertyAdvanceddwAI: TMemoField;
    qMonsterPropertyAdvanceddwStr: TMemoField;
    qMonsterPropertyAdvanceddwSta: TMemoField;
    qMonsterPropertyAdvanceddwDex: TMemoField;
    qMonsterPropertyAdvanceddwInt: TMemoField;
    qMonsterPropertyAdvanceddwHR: TMemoField;
    qMonsterPropertyAdvanceddwER: TMemoField;
    qMonsterPropertyAdvanceddwRace: TMemoField;
    qMonsterPropertyAdvanceddwBelligerence: TMemoField;
    qMonsterPropertyAdvanceddwGender: TMemoField;
    qMonsterPropertyAdvanceddwLevel: TMemoField;
    qMonsterPropertyAdvanceddwFilghtLevel: TMemoField;
    qMonsterPropertyAdvanceddwSize: TMemoField;
    qMonsterPropertyAdvanceddwClass: TMemoField;
    qMonsterPropertyAdvancedbIfPart: TMemoField;
    qMonsterPropertyAdvanceddwKarma: TMemoField;
    qMonsterPropertyAdvanceddwUseable: TMemoField;
    qMonsterPropertyAdvanceddwActionRadius: TMemoField;
    qMonsterPropertyAdvanceddwAtkMin: TMemoField;
    qMonsterPropertyAdvanceddwAtkMax: TMemoField;
    qMonsterPropertyAdvanceddwAtk1: TMemoField;
    qMonsterPropertyAdvanceddwAtk2: TMemoField;
    qMonsterPropertyAdvanceddwAtk3: TMemoField;
    qMonsterPropertyAdvanceddwHorizontalRate: TMemoField;
    qMonsterPropertyAdvanceddwVerticalRate: TMemoField;
    qMonsterPropertyAdvanceddwDiagonalRate: TMemoField;
    qMonsterPropertyAdvanceddwThrustRate: TMemoField;
    qMonsterPropertyAdvanceddwChestRate: TMemoField;
    qMonsterPropertyAdvanceddwHeadRate: TMemoField;
    qMonsterPropertyAdvanceddwArmRate: TMemoField;
    qMonsterPropertyAdvanceddwLegRate: TMemoField;
    qMonsterPropertyAdvanceddwAttackSpeed: TMemoField;
    qMonsterPropertyAdvanceddwReAttackDelay: TMemoField;
    qMonsterPropertyAdvanceddwAddHp: TMemoField;
    qMonsterPropertyAdvanceddwAddMp: TMemoField;
    qMonsterPropertyAdvanceddwNaturealArmor: TMemoField;
    qMonsterPropertyAdvancednAbrasion: TMemoField;
    qMonsterPropertyAdvancednHardness: TMemoField;
    qMonsterPropertyAdvanceddwAdjAtkDelay: TMemoField;
    qMonsterPropertyAdvancedeElementType: TMemoField;
    qMonsterPropertyAdvancedwElementAtk: TMemoField;
    qMonsterPropertyAdvanceddwHideLevel: TMemoField;
    qMonsterPropertyAdvancedfSpeed: TMemoField;
    qMonsterPropertyAdvanceddwShelter: TMemoField;
    qMonsterPropertyAdvancedbFlying: TMemoField;
    qMonsterPropertyAdvanceddwJumpIng: TMemoField;
    qMonsterPropertyAdvanceddwAirJump: TMemoField;
    qMonsterPropertyAdvancedbTaming: TMemoField;
    qMonsterPropertyAdvanceddwResisMagic: TMemoField;
    qMonsterPropertyAdvancedfResistElecricity: TMemoField;
    qMonsterPropertyAdvancedfResistFire: TMemoField;
    qMonsterPropertyAdvancedfResistWind: TMemoField;
    qMonsterPropertyAdvancedfResistWater: TMemoField;
    qMonsterPropertyAdvancedfResistEarth: TMemoField;
    qMonsterPropertyAdvanceddwCash: TMemoField;
    qMonsterPropertyAdvanceddwSourceMaterial: TMemoField;
    qMonsterPropertyAdvanceddwMaterialAmount: TMemoField;
    qMonsterPropertyAdvanceddwCohesion: TMemoField;
    qMonsterPropertyAdvanceddwHoldingTime: TMemoField;
    qMonsterPropertyAdvanceddwCorrectionValue: TMemoField;
    qMonsterPropertyAdvanceddwExpValue: TMemoField;
    qMonsterPropertyAdvancednFxpValue: TMemoField;
    qMonsterPropertyAdvancednBodyState: TMemoField;
    qMonsterPropertyAdvanceddwAddAbility: TMemoField;
    qMonsterPropertyAdvancedbKillable: TMemoField;
    qMonsterPropertyAdvanceddwVirtItem1: TMemoField;
    qMonsterPropertyAdvanceddwVirtType1: TMemoField;
    qMonsterPropertyAdvanceddwVirtItem2: TMemoField;
    qMonsterPropertyAdvanceddwVirtType2: TMemoField;
    qMonsterPropertyAdvanceddwVirtItem3: TMemoField;
    qMonsterPropertyAdvanceddwVirtType3: TMemoField;
    qMonsterPropertyAdvanceddwSndAtk1: TMemoField;
    qMonsterPropertyAdvanceddwSndAtk2: TMemoField;
    qMonsterPropertyAdvanceddwSndDie1: TMemoField;
    qMonsterPropertyAdvanceddwSndDie2: TMemoField;
    qMonsterPropertyAdvanceddwSndDmg1: TMemoField;
    qMonsterPropertyAdvanceddwSndDmg2: TMemoField;
    qMonsterPropertyAdvanceddwSndDmg3: TMemoField;
    qMonsterPropertyAdvanceddwSndIdle1: TMemoField;
    qMonsterPropertyAdvanceddwSndIdle2: TMemoField;
    qMonsterPropertyAdvancedszComment: TMemoField;
    qItem: TUniQuery;
    qItemid: TIntegerField;
    qIteminternal_id_item: TMemoField;
    qItemitem_name: TMemoField;
    qItemProperty: TUniQuery;
    qItemPropertyid: TIntegerField;
    qItemPropertyver6: TMemoField;
    qItemPropertydwID: TMemoField;
    qItemPropertyszName: TMemoField;
    qItemPropertydwNum: TMemoField;
    qItemPropertydwPackMax: TMemoField;
    qItemPropertydwItemKind1: TMemoField;
    qItemPropertydwItemKind2: TMemoField;
    qItemPropertydwItemKind3: TMemoField;
    qItemPropertydwItemJob: TMemoField;
    qItemPropertybPermanence: TMemoField;
    qItemPropertydwUseable: TMemoField;
    qItemPropertydwItemSex: TMemoField;
    qItemPropertydwCost: TMemoField;
    qItemPropertydwEndurance: TMemoField;
    qItemPropertynAbrasion: TMemoField;
    qItemPropertynMaxRepair: TMemoField;
    qItemPropertydwHanded: TMemoField;
    qItemPropertydwFlag: TMemoField;
    qItemPropertydwParts: TMemoField;
    qItemPropertydwPartsub: TMemoField;
    qItemPropertybPartFile: TMemoField;
    qItemPropertydwExclusive: TMemoField;
    qItemPropertydwBasePartsIgnore: TMemoField;
    qItemPropertydwItemLV: TMemoField;
    qItemPropertydwItemRare: TMemoField;
    qItemPropertydwShopAble: TMemoField;
    qItemPropertybLog: TMemoField;
    qItemPropertybCharged: TMemoField;
    qItemPropertydwLinkKindBullet: TMemoField;
    qItemPropertydwLinkKind: TMemoField;
    qItemPropertydwAbilityMin: TMemoField;
    qItemPropertydwAbilityMax: TMemoField;
    qItemPropertyeItemType: TMemoField;
    qItemPropertywItemEAtk: TMemoField;
    qItemPropertydwParry: TMemoField;
    qItemPropertydwBlockRating: TMemoField;
    qItemPropertydwAddSkillMin: TMemoField;
    qItemPropertydwAddSkillMax: TMemoField;
    qItemPropertydwAtkStyle: TMemoField;
    qItemPropertydwWeaponType: TMemoField;
    qItemPropertydwItemAtkOrder1: TMemoField;
    qItemPropertydwItemAtkOrder2: TMemoField;
    qItemPropertydwItemAtkOrder3: TMemoField;
    qItemPropertydwItemAtkOrder4: TMemoField;
    qItemPropertybContinuousPain: TMemoField;
    qItemPropertydwShellQuantity: TMemoField;
    qItemPropertydwRecoil: TMemoField;
    qItemPropertydwLoadingTime: TMemoField;
    qItemPropertynAdjHitRate: TMemoField;
    qItemPropertydwAttackSpeed: TMemoField;
    qItemPropertydwDmgShift: TMemoField;
    qItemPropertydwAttackRange: TMemoField;
    qItemPropertydwProbability: TMemoField;
    qItemPropertydwDestParam1: TMemoField;
    qItemPropertydwDestParam2: TMemoField;
    qItemPropertydwDestParam3: TMemoField;
    qItemPropertynAdjParamVal1: TMemoField;
    qItemPropertynAdjParamVal2: TMemoField;
    qItemPropertynAdjParamVal3: TMemoField;
    qItemPropertydwChgParamVal1: TMemoField;
    qItemPropertydwChgParamVal2: TMemoField;
    qItemPropertydwChgParamVal3: TMemoField;
    qItemPropertydwdestData1: TMemoField;
    qItemPropertydwdestData2: TMemoField;
    qItemPropertydwdestData3: TMemoField;
    qItemPropertydwactiveskill: TMemoField;
    qItemPropertydwactiveskillLv: TMemoField;
    qItemPropertydwactiveskillper: TMemoField;
    qItemPropertydwReqMp: TMemoField;
    qItemPropertydwRepFp: TMemoField;
    qItemPropertydwReqDisLV: TMemoField;
    qItemPropertydwReSkill1: TMemoField;
    qItemPropertydwReSkillLevel1: TMemoField;
    qItemPropertydwReSkill2: TMemoField;
    qItemPropertydwReSkillLevel2: TMemoField;
    qItemPropertydwSkillReadyType: TMemoField;
    qItemPropertydwSkillReady: TMemoField;
    qItemPropertydwSkillRange: TMemoField;
    qItemPropertydwSfxElemental: TMemoField;
    qItemPropertydwSfxObj: TMemoField;
    qItemPropertydwSfxObj2: TMemoField;
    qItemPropertydwSfxObj3: TMemoField;
    qItemPropertydwSfxObj4: TMemoField;
    qItemPropertydwSfxObj5: TMemoField;
    qItemPropertydwUseMotion: TMemoField;
    qItemPropertydwCircleTime: TMemoField;
    qItemPropertydwSkillTime: TMemoField;
    qItemPropertydwExeTarget: TMemoField;
    qItemPropertydwUseChance: TMemoField;
    qItemPropertydwSpellRegion: TMemoField;
    qItemPropertydwSpellType: TMemoField;
    qItemPropertydwReferStat1: TMemoField;
    qItemPropertydwReferStat2: TMemoField;
    qItemPropertydwReferTarget1: TMemoField;
    qItemPropertydwReferTarget2: TMemoField;
    qItemPropertydwReferValue1: TMemoField;
    qItemPropertydwReferValue2: TMemoField;
    qItemPropertydwSkillType: TMemoField;
    qItemPropertyfItemResistElecricity: TMemoField;
    qItemPropertyfItemResistFire: TMemoField;
    qItemPropertyfItemResistWind: TMemoField;
    qItemPropertyfItemResistWater: TMemoField;
    qItemPropertyfItemResistEarth: TMemoField;
    qItemPropertynEvildoing: TMemoField;
    qItemPropertydwExpertLV: TMemoField;
    qItemPropertyExpertMax: TMemoField;
    qItemPropertydwSubDefine: TMemoField;
    qItemPropertydwExp: TMemoField;
    qItemPropertydwComboStyle: TMemoField;
    qItemPropertyfFlightSpeed: TMemoField;
    qItemPropertyfFlightLRAngle: TMemoField;
    qItemPropertyfFlightTBAngle: TMemoField;
    qItemPropertydwFlightLimit: TMemoField;
    qItemPropertydwFFuelReMax: TMemoField;
    qItemPropertydwAFuelReMax: TMemoField;
    qItemPropertydwFuelRe: TMemoField;
    qItemPropertydwLimitLevel1: TMemoField;
    qItemPropertydwReflect: TMemoField;
    qItemPropertydwSndAttack1: TMemoField;
    qItemPropertydwSndAttack2: TMemoField;
    qItemPropertyszIcon: TMemoField;
    qItemPropertydwQuestID: TMemoField;
    qItemPropertyszTextFile: TMemoField;
    qItemPropertyszComment: TMemoField;
    UniQuery1: TUniQuery;
    UniQuery1id: TIntegerField;
    UniQuery1internal_id_item: TMemoField;
    UniQuery1item_name: TMemoField;
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
