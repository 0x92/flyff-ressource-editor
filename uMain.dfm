object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'FRE ~ [Flyff Ressource Editor] v0.4 0x92'
  ClientHeight = 1018
  ClientWidth = 1305
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 1305
    Height = 1018
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet3
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 1012
    ClientRectLeft = 2
    ClientRectRight = 1299
    ClientRectTop = 27
    object cxTabSheet1: TcxTabSheet
      Caption = 'Welcome'
      ImageIndex = 0
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Data Overview'
      ImageIndex = 1
      object cxGroupBox1: TcxGroupBox
        Left = 3
        Top = 3
        Caption = 'available Monster'
        TabOrder = 0
        Height = 190
        Width = 529
        object cxMemo1: TcxMemo
          Left = 16
          Top = 24
          Lines.Strings = (
            'cxMemo1')
          Properties.HideSelection = False
          Properties.ScrollBars = ssVertical
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 0
          Height = 113
          Width = 497
        end
        object cxButton1: TcxButton
          Left = 72
          Top = 143
          Width = 97
          Height = 25
          Caption = 'Get All Monster'
          TabOrder = 1
          OnClick = cxButton1Click
        end
        object cxButton5: TcxButton
          Left = 272
          Top = 143
          Width = 177
          Height = 25
          Caption = 'Parse internal ID / Name'
          TabOrder = 2
          OnClick = cxButton5Click
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 538
        Top = 3
        Caption = 'available Items'
        TabOrder = 1
        Height = 190
        Width = 535
        object cxMemo2: TcxMemo
          Left = 14
          Top = 24
          Lines.Strings = (
            'cxMemo1')
          Properties.HideSelection = False
          Properties.ScrollBars = ssVertical
          Style.Color = clWindow
          TabOrder = 0
          Height = 113
          Width = 497
        end
        object cxButton2: TcxButton
          Left = 88
          Top = 143
          Width = 97
          Height = 25
          Caption = 'Get All Items'
          TabOrder = 1
          OnClick = cxButton2Click
        end
        object cxButton6: TcxButton
          Left = 216
          Top = 143
          Width = 129
          Height = 25
          Caption = 'Parse internal ID / Item'
          TabOrder = 2
          OnClick = cxButton6Click
        end
      end
      object cxGroupBox3: TcxGroupBox
        Left = 3
        Top = 701
        Caption = 'Misc Functions'
        TabOrder = 2
        Height = 105
        Width = 1070
        object cxButton3: TcxButton
          Left = 16
          Top = 24
          Width = 145
          Height = 25
          Caption = 'Insert Monster into DB'
          TabOrder = 0
          OnClick = cxButton3Click
        end
        object cxButton4: TcxButton
          Left = 549
          Top = 24
          Width = 164
          Height = 25
          Caption = 'Insert Items into DB'
          TabOrder = 1
          OnClick = cxButton4Click
        end
      end
      object cxGroupBox4: TcxGroupBox
        Left = 3
        Top = 199
        Caption = 'available Monsterpropertys'
        TabOrder = 3
        Height = 242
        Width = 1118
        object cxMemo3: TcxMemo
          Left = 16
          Top = 16
          Properties.HideSelection = False
          Properties.ScrollBars = ssBoth
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 0
          Height = 113
          Width = 1097
        end
        object cxButton7: TcxButton
          Left = 16
          Top = 135
          Width = 161
          Height = 25
          Caption = 'Get all Monsterpropertys'
          TabOrder = 1
          OnClick = cxButton7Click
        end
        object cxButton8: TcxButton
          Left = 272
          Top = 135
          Width = 75
          Height = 25
          Caption = 'Parse Test'
          TabOrder = 2
          OnClick = cxButton8Click
        end
        object cxMemo4: TcxMemo
          Left = 16
          Top = 166
          TabOrder = 3
          Height = 59
          Width = 1099
        end
      end
      object cxGroupBox5: TcxGroupBox
        Left = 3
        Top = 447
        Caption = 'available Itempropertys'
        TabOrder = 4
        Height = 242
        Width = 1118
        object cxMemo5: TcxMemo
          Left = 16
          Top = 16
          Properties.HideSelection = False
          Properties.ScrollBars = ssBoth
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 0
          Height = 113
          Width = 1097
        end
        object cxButton9: TcxButton
          Left = 16
          Top = 135
          Width = 161
          Height = 25
          Caption = 'Get all Itempropertys'
          TabOrder = 1
          OnClick = cxButton9Click
        end
        object cxButton10: TcxButton
          Left = 272
          Top = 135
          Width = 75
          Height = 25
          Caption = 'Parse Test'
          TabOrder = 2
          OnClick = cxButton10Click
        end
        object cxMemo6: TcxMemo
          Left = 16
          Top = 166
          TabOrder = 3
          Height = 59
          Width = 1099
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Monster Item Droprate'
      ImageIndex = 2
      object cxGroupBox6: TcxGroupBox
        Left = 0
        Top = 2
        Caption = 'available Monster'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 1000
        Width = 1294
        object cxMonsterListbox: TcxListBox
          Left = 16
          Top = 24
          Width = 145
          Height = 545
          ItemHeight = 13
          TabOrder = 0
          OnClick = cxMonsterListboxClick
        end
        object cxGroupBox7: TcxGroupBox
          Left = 719
          Top = 24
          Caption = '~ Placeholder Monstername / Propertys ~'
          TabOrder = 1
          Height = 671
          Width = 562
          object cxMIContainerMemo: TcxMemo
            Left = 16
            Top = 31
            Properties.ScrollBars = ssVertical
            TabOrder = 0
            Height = 178
            Width = 529
          end
          object cxListBoxItem: TcxListBox
            Left = 16
            Top = 224
            Width = 506
            Height = 210
            ItemHeight = 13
            TabOrder = 1
          end
          object cxButton11: TcxButton
            Left = 71
            Top = 440
            Width = 75
            Height = 25
            Caption = 'Get all Item'
            TabOrder = 2
            OnClick = cxButton11Click
          end
        end
        object cxAdvancedGroupbox: TcxGroupBox
          Left = 167
          Top = 24
          Caption = 'Advanced'
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          Style.Color = clBtnFace
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Lao UI'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfUltraFlat
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TransparentBorder = True
          Style.IsFontAssigned = True
          StyleDisabled.BorderStyle = ebsUltraFlat
          StyleDisabled.LookAndFeel.Kind = lfUltraFlat
          StyleDisabled.LookAndFeel.NativeStyle = False
          TabOrder = 2
          Height = 409
          Width = 506
          object cxGroupBox9: TcxGroupBox
            Left = 8
            Top = 151
            Caption = 'Basic Stats'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Lao UI'
            Style.Font.Style = [fsBold]
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 0
            Height = 242
            Width = 489
            object cxLabel4: TcxLabel
              Left = 8
              Top = 63
              Caption = 'Str'
              Style.TextStyle = [fsBold]
              Style.TransparentBorder = False
            end
            object cxTextEditStr: TcxTextEdit
              Left = 38
              Top = 60
              Properties.ReadOnly = True
              TabOrder = 1
              Width = 47
            end
            object cxLabel5: TcxLabel
              Left = 141
              Top = 61
              Caption = 'Sta'
            end
            object cxTextEditSta: TcxTextEdit
              Left = 167
              Top = 59
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 47
            end
            object cxLabel6: TcxLabel
              Left = 260
              Top = 61
              Caption = 'Dex'
            end
            object cxTextEditDex: TcxTextEdit
              Left = 290
              Top = 59
              Properties.ReadOnly = True
              TabOrder = 5
              Width = 47
            end
            object cxLabel7: TcxLabel
              Left = 390
              Top = 62
              Caption = 'Int'
            end
            object cxTextEditInt: TcxTextEdit
              Left = 416
              Top = 60
              Properties.ReadOnly = True
              TabOrder = 7
              Width = 47
            end
            object cxLabel10: TcxLabel
              Left = 118
              Top = 108
              Caption = 'MinAtk'
            end
            object cxTextEditMinAtk: TcxTextEdit
              Left = 167
              Top = 107
              Properties.ReadOnly = True
              TabOrder = 9
              Width = 47
            end
            object cxLabel11: TcxLabel
              Left = 365
              Top = 108
              Caption = 'MaxAtk'
            end
            object cxTextEditMaxAtk: TcxTextEdit
              Left = 416
              Top = 107
              Properties.ReadOnly = True
              TabOrder = 11
              Width = 47
            end
            object cxLabel12: TcxLabel
              Left = 107
              Top = 135
              Caption = 'AtkSpeed'
            end
            object cxTextEditAtkSpeed: TcxTextEdit
              Left = 167
              Top = 134
              Properties.ReadOnly = True
              TabOrder = 13
              Width = 47
            end
            object cxLabel13: TcxLabel
              Left = 357
              Top = 135
              Caption = 'AtkDelay'
            end
            object cxTextEditAtkDelay: TcxTextEdit
              Left = 416
              Top = 134
              Properties.ReadOnly = True
              TabOrder = 15
              Width = 47
            end
            object cxLabel14: TcxLabel
              Left = 146
              Top = 22
              Caption = 'HP'
              Style.TextStyle = [fsBold]
              Style.TransparentBorder = False
            end
            object cxTextEditHP: TcxTextEdit
              Left = 167
              Top = 19
              Properties.ReadOnly = True
              TabOrder = 17
              Width = 47
            end
            object cxLabel15: TcxLabel
              Left = 262
              Top = 20
              Caption = 'MP'
            end
            object cxTextEditMP: TcxTextEdit
              Left = 290
              Top = 19
              Properties.ReadOnly = True
              TabOrder = 19
              Width = 47
            end
            object cxLabel16: TcxLabel
              Left = 107
              Top = 167
              Caption = 'Defense'
            end
            object cxTextEditDefense: TcxTextEdit
              Left = 167
              Top = 166
              Properties.ReadOnly = True
              TabOrder = 21
              Width = 47
            end
            object cxLabel17: TcxLabel
              Left = 362
              Top = 167
              Caption = 'Element'
            end
            object cxTextEditElement: TcxTextEdit
              Left = 416
              Top = 166
              Properties.ReadOnly = True
              TabOrder = 23
              Width = 47
            end
            object cxLabel18: TcxLabel
              Left = 107
              Top = 194
              Caption = 'EXP'
            end
            object cxTextEditExp: TcxTextEdit
              Left = 167
              Top = 193
              Properties.ReadOnly = True
              TabOrder = 25
              Width = 47
            end
            object cxCheckBoxKillable: TcxCheckBox
              Left = 403
              Top = 195
              Caption = 'Killable'
              Properties.ReadOnly = True
              TabOrder = 26
            end
          end
          object cxGroupBox10: TcxGroupBox
            Left = 8
            Top = 21
            Caption = 'Internal'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Lao UI'
            Style.Font.Style = [fsBold]
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 1
            Height = 124
            Width = 489
            object cxLabel1: TcxLabel
              Left = 8
              Top = 26
              Caption = 'ID'
            end
            object cxLabel2: TcxLabel
              Left = 8
              Top = 57
              Caption = 'Name'
            end
            object cxLabel3: TcxLabel
              Left = 8
              Top = 88
              Caption = 'AI'
            end
            object cxTextEditID: TcxTextEdit
              Left = 66
              Top = 25
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 161
            end
            object cxTextEditName: TcxTextEdit
              Left = 66
              Top = 56
              Properties.ReadOnly = True
              TabOrder = 4
              Width = 161
            end
            object cxTextEditAI: TcxTextEdit
              Left = 66
              Top = 87
              Properties.ReadOnly = True
              TabOrder = 5
              Width = 161
            end
            object cxLabel8: TcxLabel
              Left = 280
              Top = 57
              Caption = 'Level'
            end
            object cxTextEditLevel: TcxTextEdit
              Left = 322
              Top = 56
              Properties.ReadOnly = True
              TabOrder = 7
              Width = 47
            end
            object cxLabel9: TcxLabel
              Left = 280
              Top = 88
              Caption = 'Rank'
            end
            object cxTextEditRank: TcxTextEdit
              Left = 322
              Top = 87
              Properties.ReadOnly = True
              TabOrder = 9
              Width = 151
            end
            object cxLabel19: TcxLabel
              Left = 233
              Top = 26
              Caption = 'Ingame Name'
            end
            object cxTextEditIngameName: TcxTextEdit
              Left = 322
              Top = 25
              Properties.ReadOnly = True
              TabOrder = 11
              Width = 151
            end
          end
        end
        object cxEditAdvancedButton: TcxButton
          Left = 167
          Top = 439
          Width = 66
          Height = 25
          Caption = 'Unlock'
          OptionsImage.ImageIndex = 0
          OptionsImage.Images = ImageList
          TabOrder = 3
          OnClick = cxEditAdvancedButtonClick
        end
        object cxSaveAdvanceButton: TcxButton
          Left = 607
          Top = 439
          Width = 66
          Height = 25
          Caption = 'Save'
          Enabled = False
          OptionsImage.ImageIndex = 2
          OptionsImage.Images = ImageList
          TabOrder = 4
          OnClick = cxSaveAdvanceButtonClick
        end
        object cxGroupBoxItemDrops: TcxGroupBox
          Left = 167
          Top = 470
          Caption = 'Item Drops'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          TabOrder = 5
          Height = 435
          Width = 506
          object cxGroupBox8: TcxGroupBox
            Left = 8
            Top = 24
            Caption = 'Item Category'
            TabOrder = 0
            Height = 204
            Width = 489
            object cxListItemDropGroupView: TcxListView
              Left = 6
              Top = 22
              Width = 478
              Height = 173
              Columns = <
                item
                  AutoSize = True
                  Caption = 'Category'
                end
                item
                  AutoSize = True
                  Caption = 'ItemUniqueMin'
                end
                item
                  AutoSize = True
                  Caption = 'ItemUniqueMax'
                end>
              TabOrder = 0
              ViewStyle = vsReport
            end
          end
          object cxListItemDropView: TcxGroupBox
            Left = 8
            Top = 234
            Caption = 'Single Items'
            TabOrder = 1
            Height = 167
            Width = 489
            object cxListView1: TcxListView
              Left = 8
              Top = 24
              Width = 478
              Height = 83
              Columns = <
                item
                  AutoSize = True
                  Caption = 'Item'
                end
                item
                  AutoSize = True
                  Caption = 'Droprate %'
                end
                item
                  AutoSize = True
                  Caption = 'Level'
                end
                item
                  AutoSize = True
                  Caption = 'Amount'
                end>
              TabOrder = 0
              ViewStyle = vsReport
            end
          end
        end
        object cxTextEditSearch: TcxTextEdit
          Left = 16
          Top = 572
          TabOrder = 6
          TextHint = '...'
          Width = 145
        end
        object cxButton12: TcxButton
          Left = 735
          Top = 752
          Width = 75
          Height = 25
          Caption = 'cxButton12'
          TabOrder = 7
          OnClick = cxButton12Click
        end
      end
    end
  end
  object cxProgressBar1: TcxProgressBar
    Left = 288
    Top = 4
    Properties.PeakValue = 10.000000000000000000
    TabOrder = 1
    Width = 787
  end
  object dxSkinController: TdxSkinController
    NativeStyle = False
    SkinName = 'DevExpressStyle'
    Left = 1040
    Top = 616
  end
  object MainMenu: TMainMenu
    Left = 976
    Top = 616
    object File1: TMenuItem
      Caption = 'File'
    end
    object Settings1: TMenuItem
      Caption = 'Settings'
      OnClick = Settings1Click
    end
    object About1: TMenuItem
      Caption = 'Language'
      object English1: TMenuItem
        Caption = 'English'
        OnClick = English1Click
      end
      object Deutsch1: TMenuItem
        Caption = 'Deutsch'
        OnClick = Deutsch1Click
      end
    end
    object About2: TMenuItem
      Caption = 'About'
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 1090
    Top = 115
  end
  object ImageList: TImageList
    ColorDepth = cd32Bit
    Left = 1005
    Top = 501
    Bitmap = {
      494C010103000800300010001000FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000F1D950017
      2CB30014269E00162793001D2D9500273BA200182FBD00182FBC00182FBC0018
      2FBC00182FBC000D1A8B000000000000000000000000000F1D9500172CB30014
      269E00162793001D2D9500273BA200182FBD00182FBC00182FBC00182FBC0018
      2FBC000D1A8B0000000000000000000000000000000000000000000000510000
      00E70101018A0101018A0101018A0101018A0101018A0101018A000000E70000
      00E7000000E7000000E7000000AD000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000001526A0A9ED
      FFFFA4E7FCFF9CDFF8FF94D6F4FF8BCFF0FF65BCDEFF65C2E4FF65CAECFF65D2
      F4FF65DAFCFF001526A0000000000000000000000000001526A0A9EDFFFFA4E7
      FCFF9CDFF8FF94D6F4FF8BCFF0FF65BCDEFF65C2E4FF65CAECFF65D2F4FF65DA
      FCFF001526A0000000000000000000000000000000000000004C000000DB2B2B
      2BF7B9ABABFF424242FF424242FFB5A7A7FFB5A7A7FFB9ABABFF424242FF5454
      54FF515151FF676767FF000000DB000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000016268D65AA
      CAFF67ACCEFF6BB0D2FF6DB1D3FF69AED0FF549DBFFF54A0C2FF54A5CAFF54AB
      D3FF59B8DEFF0016268F0000000000000000000000000016268D65AACAFF67AC
      CEFF6BB0D2FF6DB1D3FF69AED0FF549DBFFF54A0C2FF54A5CAFF54ABD3FF59B8
      DEFF0016268F00000000000000000000000000000000000000CE656565FF3E3E
      3EFFBAB1B1FF3E3E3EFF3E3E3EFFB1A8A8FFB1A8A8FFBAB1B1FF3E3E3EFF5454
      54FF494949FF696969FF000000CE000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000001C2C87A5E9
      FFFF9CDFFAFF94D7F4FF8CCEEEFF83C7E8FF5DB4D7FF5DBADEFF5DC2E8FF5DCA
      F2FF61D6FBFF001D2D8A000000000000000000000000001C2C87A5E9FFFF9CDF
      FAFF94D7F4FF8CCEEEFF83C7E8FF5DB4D7FF5DBADEFF5DC2E8FF5DCAF2FF61D6
      FBFF001D2D8A00000000000000000000000000000000000000C9636363FF3A3A
      3AFFC0BBBBFF262626FF262626FFB7B2B2FFB7B2B2FFC0BBBBFF3A3A3AFF5454
      54FF414141FF6B6B6BFF000000C9000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000124368A65AA
      CAFF67ACCEFF6BB0D2FF6DB1D3FF69AED0FF549DBFFF54A0C2FF54A5CAFF54AB
      D3FF59B8DEFF0125388F0000000000000000000000000124368A65AACAFF67AC
      CEFF6BB0D2FF6DB1D3FF69AED0FF549DBFFF54A0C2FF54A5CAFF54ABD3FF59B8
      DEFF0125388F00000000000000000000000000000000000000C5666666FF3636
      36FFC8C7C7FFC3C2C2FFC3C2C2FFC3C2C2FFC3C2C2FFC8C7C7FF363636FF5454
      54FF393939FF6E6E6EFF000000C5000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000126388BA5E9
      FFFF9CDFFAFF94D7F4FF8CCEEEFF83C7E8FF5DB4D7FF5DBADEFF5DC2E8FF5DCA
      F2FF61D6FBFF01273B900000000000000000000000000126388BA5E9FFFF9CDF
      FAFF94D7F4FF8CCEEEFF83C7E8FF5DB4D7FF5DBADEFF5DC2E8FF5DCAF2FF61D6
      FBFF01273B9000000000000000000000000000000000000000C16A6A6AFF3333
      33FF323232FF323232FF323232FF323232FF323232FF323232FF333333FF3333
      33FF333333FF727272FF000000C1000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000126388B65AA
      CAFF67ACCEFF6BB0D2FF6DB1D3FF69AED0FF549DBFFF54A0C2FF54A5CAFF54AB
      D3FF59B8DEFF01273B900000000000000000000000000126388B65AACAFF67AC
      CEFF6BB0D2FF6DB1D3FF69AED0FF549DBFFF54A0C2FF54A5CAFF54ABD3FF59B8
      DEFF01273B9000000000000000000000000000000000000000BE6D6D6DFF6464
      64FF646464FF646464FF646464FF646464FF646464FF646464FF646464FF6464
      64FF646464FF6D6D6DFF000000BE000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000126388BA5E9
      FFFF9CDFFAFF94D7F4FF8CCEEEFF83C7E8FF5DB4D7FF5DBADEFF5DC2E8FF5DCA
      F2FF61D6FBFF01273B900000000000000000000000000126388BA5E9FFFF9CDF
      FAFF94D7F4FF8CCEEEFF83C7E8FF5DB4D7FF5DBADEFF5DC2E8FF5DCAF2FF61D6
      FBFF01273B9000000000000000000000000000000000000000BB717171FFD4D4
      C9FFF4F4E4FFF4F4E4FFF4F4E4FFF4F4E4FFF4F4E4FFF4F4E4FFF4F4E4FFF4F4
      E4FFD4D4C9FF717171FF000000BB000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000001520690127
      3A8F002639A864AACCFF002E45AD002E43A9003652CA002E45AD4A91B5FF0026
      39A8002B409B0016226D0000000000000000000000000015206901273A8F0028
      3D95002A3F9C002C41A2002E43A9003652CA002639A84A91B5FF002E45AD002B
      409B0016226D00000000000000000000000000000000000000B8747474FFF6F6
      E9FFECECDFFFECECDFFFECECDFFFECECDFFFECECDFFFECECDFFFECECDFFFECEC
      DFFFF6F6E9FF747474FF000000B8000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000263AA375BADBFF003047B20000000000000000003047B277BCDDFF0026
      3AA3000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000263AA377BCDDFF003047B20000
      000000000000001B288600263AA30015217A00000000000000B5787878FFF8F8
      EFFFF1F1E7FFF1F1E7FFF1F1E7FFF1F1E7FFF1F1E7FFF1F1E7FFF1F1E7FFF1F1
      E7FFF8F8EFFF787878FF000000B5000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000024369782C5E2FD02344DB9000000000000000002344DB982C5E2FD0024
      3697000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000024369782C5E2FD02344DB90000
      00000000000002344DB982C5E2FD0024369700000000000000B27B7B7BFFFBFB
      F5FFF6F6F0FFF6F6F0FFF6F6F0FFF6F6F0FFF6F6F0FFF6F6F0FFF6F6F0FFF6F6
      F0FFFBFBF5FF7B7B7BFF000000B2000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000001A267A64A2BBE6275F7CD5001D2B91001D2B91275F7CD564A2BBE6001A
      267A000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000001A267A64A2BBE6275F7CD5001D
      2B91001D2B91275F7CD564A2BBE6001A267A00000000000000B07D7D7DFFFEFE
      FBFFFBFBF8FFFBFBF8FFFBFBF8FFFBFBF8FFFBFBF8FFFBFBF8FFFBFBF8FFFBFB
      F8FFFEFEFBFF7D7D7DFF000000B0000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000507350A33479B619FB6E15898B0EA5898B0EA619FB6E10A33479B0005
      0735000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000507350A33479B619FB6E15898
      B0EA5898B0EA619FB6E10A33479B0005073500000000000000AE848484FFFFFF
      FFFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFF
      FEFFFFFFFFFF848484FF000000AE000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000005073300192572012437890124378900192572000507330000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000050733001925720124
      3789012437890019257200050733000000000000000000000081000000AC1414
      0D6614140D6614140D6614140D6614140D6614140D6614140D6614140D661414
      0D6614140D66000000AC00000081000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
end
