CREATE TABLE "FRE_Item" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"internal_id_item"	TEXT NOT NULL UNIQUE,
	"item_name"	TEXT NOT NULL
)