program FRE;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {MainForm},
  uSettings in 'uSettings.pas' {SettingsForm},
  uParser in 'uParser.pas',
  uDM in 'uDM.pas' {DM: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TSettingsForm, SettingsForm);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
