object DM: TDM
  OldCreateOrder = False
  Height = 438
  Width = 799
  object UniConnection: TUniConnection
    ProviderName = 'SQLite'
    Database = 
      'D:\Dateien\Entwicklung\Projekte\Delphi\flyff-ressource-editor\Wi' +
      'n32\Debug\master.db'
    Connected = True
    Left = 64
    Top = 72
  end
  object qMasterDBTransfer: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_Monster;')
    Left = 80
    Top = 256
  end
  object SQLiteUniProvider: TSQLiteUniProvider
    Left = 160
    Top = 72
  end
  object qMonster: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_Monster')
    Active = True
    Left = 328
    Top = 136
    object qMonsterid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
    end
    object qMonsterinternal_id: TMemoField
      FieldName = 'internal_id'
      BlobType = ftMemo
    end
    object qMonsterMonstername: TMemoField
      FieldName = 'Monstername'
      BlobType = ftMemo
    end
  end
  object MonsterDataSource: TUniDataSource
    DataSet = qMonster
    Left = 328
    Top = 200
  end
  object qMonsterProperty: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_MonsterProperty')
    Left = 472
    Top = 136
    object qMonsterPropertyid: TIntegerField
      FieldName = 'id'
    end
    object qMonsterPropertydwID: TMemoField
      FieldName = 'dwID'
      BlobType = ftMemo
    end
    object qMonsterPropertyszName: TMemoField
      FieldName = 'szName'
      BlobType = ftMemo
    end
  end
  object qMonsterPropertyAdvanced: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_MonsterProperty')
    Left = 480
    Top = 216
    object qMonsterPropertyAdvancedid: TIntegerField
      FieldName = 'id'
    end
    object qMonsterPropertyAdvanceddwID: TMemoField
      FieldName = 'dwID'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedszName: TMemoField
      FieldName = 'szName'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAI: TMemoField
      FieldName = 'dwAI'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwStr: TMemoField
      FieldName = 'dwStr'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSta: TMemoField
      FieldName = 'dwSta'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwDex: TMemoField
      FieldName = 'dwDex'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwInt: TMemoField
      FieldName = 'dwInt'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwHR: TMemoField
      FieldName = 'dwHR'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwER: TMemoField
      FieldName = 'dwER'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwRace: TMemoField
      FieldName = 'dwRace'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwBelligerence: TMemoField
      FieldName = 'dwBelligerence'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwGender: TMemoField
      FieldName = 'dwGender'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwLevel: TMemoField
      FieldName = 'dwLevel'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwFilghtLevel: TMemoField
      FieldName = 'dwFilghtLevel'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSize: TMemoField
      FieldName = 'dwSize'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwClass: TMemoField
      FieldName = 'dwClass'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedbIfPart: TMemoField
      FieldName = 'bIfPart'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwKarma: TMemoField
      FieldName = 'dwKarma'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwUseable: TMemoField
      FieldName = 'dwUseable'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwActionRadius: TMemoField
      FieldName = 'dwActionRadius'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAtkMin: TMemoField
      FieldName = 'dwAtkMin'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAtkMax: TMemoField
      FieldName = 'dwAtkMax'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAtk1: TMemoField
      FieldName = 'dwAtk1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAtk2: TMemoField
      FieldName = 'dwAtk2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAtk3: TMemoField
      FieldName = 'dwAtk3'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwHorizontalRate: TMemoField
      FieldName = 'dwHorizontalRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVerticalRate: TMemoField
      FieldName = 'dwVerticalRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwDiagonalRate: TMemoField
      FieldName = 'dwDiagonalRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwThrustRate: TMemoField
      FieldName = 'dwThrustRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwChestRate: TMemoField
      FieldName = 'dwChestRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwHeadRate: TMemoField
      FieldName = 'dwHeadRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwArmRate: TMemoField
      FieldName = 'dwArmRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwLegRate: TMemoField
      FieldName = 'dwLegRate'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAttackSpeed: TMemoField
      FieldName = 'dwAttackSpeed'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwReAttackDelay: TMemoField
      FieldName = 'dwReAttackDelay'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAddHp: TMemoField
      FieldName = 'dwAddHp'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAddMp: TMemoField
      FieldName = 'dwAddMp'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwNaturealArmor: TMemoField
      FieldName = 'dwNaturealArmor'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancednAbrasion: TMemoField
      FieldName = 'nAbrasion'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancednHardness: TMemoField
      FieldName = 'nHardness'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAdjAtkDelay: TMemoField
      FieldName = 'dwAdjAtkDelay'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedeElementType: TMemoField
      FieldName = 'eElementType'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedwElementAtk: TMemoField
      FieldName = 'wElementAtk'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwHideLevel: TMemoField
      FieldName = 'dwHideLevel'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedfSpeed: TMemoField
      FieldName = 'fSpeed'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwShelter: TMemoField
      FieldName = 'dwShelter'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedbFlying: TMemoField
      FieldName = 'bFlying'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwJumpIng: TMemoField
      FieldName = 'dwJumpIng'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAirJump: TMemoField
      FieldName = 'dwAirJump'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedbTaming: TMemoField
      FieldName = 'bTaming'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwResisMagic: TMemoField
      FieldName = 'dwResisMagic'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedfResistElecricity: TMemoField
      FieldName = 'fResistElecricity'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedfResistFire: TMemoField
      FieldName = 'fResistFire'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedfResistWind: TMemoField
      FieldName = 'fResistWind'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedfResistWater: TMemoField
      FieldName = 'fResistWater'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedfResistEarth: TMemoField
      FieldName = 'fResistEarth'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwCash: TMemoField
      FieldName = 'dwCash'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSourceMaterial: TMemoField
      FieldName = 'dwSourceMaterial'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwMaterialAmount: TMemoField
      FieldName = 'dwMaterialAmount'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwCohesion: TMemoField
      FieldName = 'dwCohesion'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwHoldingTime: TMemoField
      FieldName = 'dwHoldingTime'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwCorrectionValue: TMemoField
      FieldName = 'dwCorrectionValue'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwExpValue: TMemoField
      FieldName = 'dwExpValue'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancednFxpValue: TMemoField
      FieldName = 'nFxpValue'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancednBodyState: TMemoField
      FieldName = 'nBodyState'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwAddAbility: TMemoField
      FieldName = 'dwAddAbility'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedbKillable: TMemoField
      FieldName = 'bKillable'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVirtItem1: TMemoField
      FieldName = 'dwVirtItem1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVirtType1: TMemoField
      FieldName = 'dwVirtType1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVirtItem2: TMemoField
      FieldName = 'dwVirtItem2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVirtType2: TMemoField
      FieldName = 'dwVirtType2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVirtItem3: TMemoField
      FieldName = 'dwVirtItem3'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwVirtType3: TMemoField
      FieldName = 'dwVirtType3'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndAtk1: TMemoField
      FieldName = 'dwSndAtk1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndAtk2: TMemoField
      FieldName = 'dwSndAtk2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndDie1: TMemoField
      FieldName = 'dwSndDie1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndDie2: TMemoField
      FieldName = 'dwSndDie2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndDmg1: TMemoField
      FieldName = 'dwSndDmg1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndDmg2: TMemoField
      FieldName = 'dwSndDmg2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndDmg3: TMemoField
      FieldName = 'dwSndDmg3'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndIdle1: TMemoField
      FieldName = 'dwSndIdle1'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvanceddwSndIdle2: TMemoField
      FieldName = 'dwSndIdle2'
      BlobType = ftMemo
    end
    object qMonsterPropertyAdvancedszComment: TMemoField
      FieldName = 'szComment'
      BlobType = ftMemo
    end
  end
  object qItem: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_Item')
    Left = 640
    Top = 144
    object qItemid: TIntegerField
      FieldName = 'id'
    end
    object qIteminternal_id_item: TMemoField
      FieldName = 'internal_id_item'
      BlobType = ftMemo
    end
    object qItemitem_name: TMemoField
      FieldName = 'item_name'
      BlobType = ftMemo
    end
  end
  object qItemProperty: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_ItemProperty')
    Left = 728
    Top = 144
    object qItemPropertyid: TIntegerField
      FieldName = 'id'
    end
    object qItemPropertyver6: TMemoField
      FieldName = 'ver6'
      BlobType = ftMemo
    end
    object qItemPropertydwID: TMemoField
      FieldName = 'dwID'
      BlobType = ftMemo
    end
    object qItemPropertyszName: TMemoField
      FieldName = 'szName'
      BlobType = ftMemo
    end
    object qItemPropertydwNum: TMemoField
      FieldName = 'dwNum'
      BlobType = ftMemo
    end
    object qItemPropertydwPackMax: TMemoField
      FieldName = 'dwPackMax'
      BlobType = ftMemo
    end
    object qItemPropertydwItemKind1: TMemoField
      FieldName = 'dwItemKind1'
      BlobType = ftMemo
    end
    object qItemPropertydwItemKind2: TMemoField
      FieldName = 'dwItemKind2'
      BlobType = ftMemo
    end
    object qItemPropertydwItemKind3: TMemoField
      FieldName = 'dwItemKind3'
      BlobType = ftMemo
    end
    object qItemPropertydwItemJob: TMemoField
      FieldName = 'dwItemJob'
      BlobType = ftMemo
    end
    object qItemPropertybPermanence: TMemoField
      FieldName = 'bPermanence'
      BlobType = ftMemo
    end
    object qItemPropertydwUseable: TMemoField
      FieldName = 'dwUseable'
      BlobType = ftMemo
    end
    object qItemPropertydwItemSex: TMemoField
      FieldName = 'dwItemSex'
      BlobType = ftMemo
    end
    object qItemPropertydwCost: TMemoField
      FieldName = 'dwCost'
      BlobType = ftMemo
    end
    object qItemPropertydwEndurance: TMemoField
      FieldName = 'dwEndurance'
      BlobType = ftMemo
    end
    object qItemPropertynAbrasion: TMemoField
      FieldName = 'nAbrasion'
      BlobType = ftMemo
    end
    object qItemPropertynMaxRepair: TMemoField
      FieldName = 'nMaxRepair'
      BlobType = ftMemo
    end
    object qItemPropertydwHanded: TMemoField
      FieldName = 'dwHanded'
      BlobType = ftMemo
    end
    object qItemPropertydwFlag: TMemoField
      FieldName = 'dwFlag'
      BlobType = ftMemo
    end
    object qItemPropertydwParts: TMemoField
      FieldName = 'dwParts'
      BlobType = ftMemo
    end
    object qItemPropertydwPartsub: TMemoField
      FieldName = 'dwPartsub'
      BlobType = ftMemo
    end
    object qItemPropertybPartFile: TMemoField
      FieldName = 'bPartFile'
      BlobType = ftMemo
    end
    object qItemPropertydwExclusive: TMemoField
      FieldName = 'dwExclusive'
      BlobType = ftMemo
    end
    object qItemPropertydwBasePartsIgnore: TMemoField
      FieldName = 'dwBasePartsIgnore'
      BlobType = ftMemo
    end
    object qItemPropertydwItemLV: TMemoField
      FieldName = 'dwItemLV'
      BlobType = ftMemo
    end
    object qItemPropertydwItemRare: TMemoField
      FieldName = 'dwItemRare'
      BlobType = ftMemo
    end
    object qItemPropertydwShopAble: TMemoField
      FieldName = 'dwShopAble'
      BlobType = ftMemo
    end
    object qItemPropertybLog: TMemoField
      FieldName = 'bLog'
      BlobType = ftMemo
    end
    object qItemPropertybCharged: TMemoField
      FieldName = 'bCharged'
      BlobType = ftMemo
    end
    object qItemPropertydwLinkKindBullet: TMemoField
      FieldName = 'dwLinkKindBullet'
      BlobType = ftMemo
    end
    object qItemPropertydwLinkKind: TMemoField
      FieldName = 'dwLinkKind'
      BlobType = ftMemo
    end
    object qItemPropertydwAbilityMin: TMemoField
      FieldName = 'dwAbilityMin'
      BlobType = ftMemo
    end
    object qItemPropertydwAbilityMax: TMemoField
      FieldName = 'dwAbilityMax'
      BlobType = ftMemo
    end
    object qItemPropertyeItemType: TMemoField
      FieldName = 'eItemType'
      BlobType = ftMemo
    end
    object qItemPropertywItemEAtk: TMemoField
      FieldName = 'wItemEAtk'
      BlobType = ftMemo
    end
    object qItemPropertydwParry: TMemoField
      FieldName = 'dwParry'
      BlobType = ftMemo
    end
    object qItemPropertydwBlockRating: TMemoField
      FieldName = 'dwBlockRating'
      BlobType = ftMemo
    end
    object qItemPropertydwAddSkillMin: TMemoField
      FieldName = 'dwAddSkillMin'
      BlobType = ftMemo
    end
    object qItemPropertydwAddSkillMax: TMemoField
      FieldName = 'dwAddSkillMax'
      BlobType = ftMemo
    end
    object qItemPropertydwAtkStyle: TMemoField
      FieldName = 'dwAtkStyle'
      BlobType = ftMemo
    end
    object qItemPropertydwWeaponType: TMemoField
      FieldName = 'dwWeaponType'
      BlobType = ftMemo
    end
    object qItemPropertydwItemAtkOrder1: TMemoField
      FieldName = 'dwItemAtkOrder1'
      BlobType = ftMemo
    end
    object qItemPropertydwItemAtkOrder2: TMemoField
      FieldName = 'dwItemAtkOrder2'
      BlobType = ftMemo
    end
    object qItemPropertydwItemAtkOrder3: TMemoField
      FieldName = 'dwItemAtkOrder3'
      BlobType = ftMemo
    end
    object qItemPropertydwItemAtkOrder4: TMemoField
      FieldName = 'dwItemAtkOrder4'
      BlobType = ftMemo
    end
    object qItemPropertybContinuousPain: TMemoField
      FieldName = 'bContinuousPain'
      BlobType = ftMemo
    end
    object qItemPropertydwShellQuantity: TMemoField
      FieldName = 'dwShellQuantity'
      BlobType = ftMemo
    end
    object qItemPropertydwRecoil: TMemoField
      FieldName = 'dwRecoil'
      BlobType = ftMemo
    end
    object qItemPropertydwLoadingTime: TMemoField
      FieldName = 'dwLoadingTime'
      BlobType = ftMemo
    end
    object qItemPropertynAdjHitRate: TMemoField
      FieldName = 'nAdjHitRate'
      BlobType = ftMemo
    end
    object qItemPropertydwAttackSpeed: TMemoField
      FieldName = 'dwAttackSpeed'
      BlobType = ftMemo
    end
    object qItemPropertydwDmgShift: TMemoField
      FieldName = 'dwDmgShift'
      BlobType = ftMemo
    end
    object qItemPropertydwAttackRange: TMemoField
      FieldName = 'dwAttackRange'
      BlobType = ftMemo
    end
    object qItemPropertydwProbability: TMemoField
      FieldName = 'dwProbability'
      BlobType = ftMemo
    end
    object qItemPropertydwDestParam1: TMemoField
      FieldName = 'dwDestParam1'
      BlobType = ftMemo
    end
    object qItemPropertydwDestParam2: TMemoField
      FieldName = 'dwDestParam2'
      BlobType = ftMemo
    end
    object qItemPropertydwDestParam3: TMemoField
      FieldName = 'dwDestParam3'
      BlobType = ftMemo
    end
    object qItemPropertynAdjParamVal1: TMemoField
      FieldName = 'nAdjParamVal1'
      BlobType = ftMemo
    end
    object qItemPropertynAdjParamVal2: TMemoField
      FieldName = 'nAdjParamVal2'
      BlobType = ftMemo
    end
    object qItemPropertynAdjParamVal3: TMemoField
      FieldName = 'nAdjParamVal3'
      BlobType = ftMemo
    end
    object qItemPropertydwChgParamVal1: TMemoField
      FieldName = 'dwChgParamVal1'
      BlobType = ftMemo
    end
    object qItemPropertydwChgParamVal2: TMemoField
      FieldName = 'dwChgParamVal2'
      BlobType = ftMemo
    end
    object qItemPropertydwChgParamVal3: TMemoField
      FieldName = 'dwChgParamVal3'
      BlobType = ftMemo
    end
    object qItemPropertydwdestData1: TMemoField
      FieldName = 'dwdestData1'
      BlobType = ftMemo
    end
    object qItemPropertydwdestData2: TMemoField
      FieldName = 'dwdestData2'
      BlobType = ftMemo
    end
    object qItemPropertydwdestData3: TMemoField
      FieldName = 'dwdestData3'
      BlobType = ftMemo
    end
    object qItemPropertydwactiveskill: TMemoField
      FieldName = 'dwactiveskill'
      BlobType = ftMemo
    end
    object qItemPropertydwactiveskillLv: TMemoField
      FieldName = 'dwactiveskillLv'
      BlobType = ftMemo
    end
    object qItemPropertydwactiveskillper: TMemoField
      FieldName = 'dwactiveskillper'
      BlobType = ftMemo
    end
    object qItemPropertydwReqMp: TMemoField
      FieldName = 'dwReqMp'
      BlobType = ftMemo
    end
    object qItemPropertydwRepFp: TMemoField
      FieldName = 'dwRepFp'
      BlobType = ftMemo
    end
    object qItemPropertydwReqDisLV: TMemoField
      FieldName = 'dwReqDisLV'
      BlobType = ftMemo
    end
    object qItemPropertydwReSkill1: TMemoField
      FieldName = 'dwReSkill1'
      BlobType = ftMemo
    end
    object qItemPropertydwReSkillLevel1: TMemoField
      FieldName = 'dwReSkillLevel1'
      BlobType = ftMemo
    end
    object qItemPropertydwReSkill2: TMemoField
      FieldName = 'dwReSkill2'
      BlobType = ftMemo
    end
    object qItemPropertydwReSkillLevel2: TMemoField
      FieldName = 'dwReSkillLevel2'
      BlobType = ftMemo
    end
    object qItemPropertydwSkillReadyType: TMemoField
      FieldName = 'dwSkillReadyType'
      BlobType = ftMemo
    end
    object qItemPropertydwSkillReady: TMemoField
      FieldName = 'dwSkillReady'
      BlobType = ftMemo
    end
    object qItemPropertydwSkillRange: TMemoField
      FieldName = 'dwSkillRange'
      BlobType = ftMemo
    end
    object qItemPropertydwSfxElemental: TMemoField
      FieldName = 'dwSfxElemental'
      BlobType = ftMemo
    end
    object qItemPropertydwSfxObj: TMemoField
      FieldName = 'dwSfxObj'
      BlobType = ftMemo
    end
    object qItemPropertydwSfxObj2: TMemoField
      FieldName = 'dwSfxObj2'
      BlobType = ftMemo
    end
    object qItemPropertydwSfxObj3: TMemoField
      FieldName = 'dwSfxObj3'
      BlobType = ftMemo
    end
    object qItemPropertydwSfxObj4: TMemoField
      FieldName = 'dwSfxObj4'
      BlobType = ftMemo
    end
    object qItemPropertydwSfxObj5: TMemoField
      FieldName = 'dwSfxObj5'
      BlobType = ftMemo
    end
    object qItemPropertydwUseMotion: TMemoField
      FieldName = 'dwUseMotion'
      BlobType = ftMemo
    end
    object qItemPropertydwCircleTime: TMemoField
      FieldName = 'dwCircleTime'
      BlobType = ftMemo
    end
    object qItemPropertydwSkillTime: TMemoField
      FieldName = 'dwSkillTime'
      BlobType = ftMemo
    end
    object qItemPropertydwExeTarget: TMemoField
      FieldName = 'dwExeTarget'
      BlobType = ftMemo
    end
    object qItemPropertydwUseChance: TMemoField
      FieldName = 'dwUseChance'
      BlobType = ftMemo
    end
    object qItemPropertydwSpellRegion: TMemoField
      FieldName = 'dwSpellRegion'
      BlobType = ftMemo
    end
    object qItemPropertydwSpellType: TMemoField
      FieldName = 'dwSpellType'
      BlobType = ftMemo
    end
    object qItemPropertydwReferStat1: TMemoField
      FieldName = 'dwReferStat1'
      BlobType = ftMemo
    end
    object qItemPropertydwReferStat2: TMemoField
      FieldName = 'dwReferStat2'
      BlobType = ftMemo
    end
    object qItemPropertydwReferTarget1: TMemoField
      FieldName = 'dwReferTarget1'
      BlobType = ftMemo
    end
    object qItemPropertydwReferTarget2: TMemoField
      FieldName = 'dwReferTarget2'
      BlobType = ftMemo
    end
    object qItemPropertydwReferValue1: TMemoField
      FieldName = 'dwReferValue1'
      BlobType = ftMemo
    end
    object qItemPropertydwReferValue2: TMemoField
      FieldName = 'dwReferValue2'
      BlobType = ftMemo
    end
    object qItemPropertydwSkillType: TMemoField
      FieldName = 'dwSkillType'
      BlobType = ftMemo
    end
    object qItemPropertyfItemResistElecricity: TMemoField
      FieldName = 'fItemResistElecricity'
      BlobType = ftMemo
    end
    object qItemPropertyfItemResistFire: TMemoField
      FieldName = 'fItemResistFire'
      BlobType = ftMemo
    end
    object qItemPropertyfItemResistWind: TMemoField
      FieldName = 'fItemResistWind'
      BlobType = ftMemo
    end
    object qItemPropertyfItemResistWater: TMemoField
      FieldName = 'fItemResistWater'
      BlobType = ftMemo
    end
    object qItemPropertyfItemResistEarth: TMemoField
      FieldName = 'fItemResistEarth'
      BlobType = ftMemo
    end
    object qItemPropertynEvildoing: TMemoField
      FieldName = 'nEvildoing'
      BlobType = ftMemo
    end
    object qItemPropertydwExpertLV: TMemoField
      FieldName = 'dwExpertLV'
      BlobType = ftMemo
    end
    object qItemPropertyExpertMax: TMemoField
      FieldName = 'ExpertMax'
      BlobType = ftMemo
    end
    object qItemPropertydwSubDefine: TMemoField
      FieldName = 'dwSubDefine'
      BlobType = ftMemo
    end
    object qItemPropertydwExp: TMemoField
      FieldName = 'dwExp'
      BlobType = ftMemo
    end
    object qItemPropertydwComboStyle: TMemoField
      FieldName = 'dwComboStyle'
      BlobType = ftMemo
    end
    object qItemPropertyfFlightSpeed: TMemoField
      FieldName = 'fFlightSpeed'
      BlobType = ftMemo
    end
    object qItemPropertyfFlightLRAngle: TMemoField
      FieldName = 'fFlightLRAngle'
      BlobType = ftMemo
    end
    object qItemPropertyfFlightTBAngle: TMemoField
      FieldName = 'fFlightTBAngle'
      BlobType = ftMemo
    end
    object qItemPropertydwFlightLimit: TMemoField
      FieldName = 'dwFlightLimit'
      BlobType = ftMemo
    end
    object qItemPropertydwFFuelReMax: TMemoField
      FieldName = 'dwFFuelReMax'
      BlobType = ftMemo
    end
    object qItemPropertydwAFuelReMax: TMemoField
      FieldName = 'dwAFuelReMax'
      BlobType = ftMemo
    end
    object qItemPropertydwFuelRe: TMemoField
      FieldName = 'dwFuelRe'
      BlobType = ftMemo
    end
    object qItemPropertydwLimitLevel1: TMemoField
      FieldName = 'dwLimitLevel1'
      BlobType = ftMemo
    end
    object qItemPropertydwReflect: TMemoField
      FieldName = 'dwReflect'
      BlobType = ftMemo
    end
    object qItemPropertydwSndAttack1: TMemoField
      FieldName = 'dwSndAttack1'
      BlobType = ftMemo
    end
    object qItemPropertydwSndAttack2: TMemoField
      FieldName = 'dwSndAttack2'
      BlobType = ftMemo
    end
    object qItemPropertyszIcon: TMemoField
      FieldName = 'szIcon'
      BlobType = ftMemo
    end
    object qItemPropertydwQuestID: TMemoField
      FieldName = 'dwQuestID'
      BlobType = ftMemo
    end
    object qItemPropertyszTextFile: TMemoField
      FieldName = 'szTextFile'
      BlobType = ftMemo
    end
    object qItemPropertyszComment: TMemoField
      FieldName = 'szComment'
      BlobType = ftMemo
    end
  end
  object UniQuery1: TUniQuery
    Connection = UniConnection
    SQL.Strings = (
      'SELECT * FROM FRE_Item')
    Left = 424
    Top = 376
    object UniQuery1id: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
    end
    object UniQuery1internal_id_item: TMemoField
      FieldName = 'internal_id_item'
      BlobType = ftMemo
    end
    object UniQuery1item_name: TMemoField
      FieldName = 'item_name'
      BlobType = ftMemo
    end
  end
end
