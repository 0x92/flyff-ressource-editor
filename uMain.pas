unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxSkinsCore, dxSkinDevExpressDarkStyle,
  cxClasses, cxLookAndFeels, dxSkinsForm, dxSkinDevExpressStyle,
  dxSkinVisualStudio2013Dark, cxGraphics, cxControls, cxLookAndFeelPainters,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC, Vcl.Menus, IniFiles, uSettings,
  cxContainer, cxEdit, cxGroupBox, uParser, cxTextEdit, cxMemo, Vcl.StdCtrls,
  cxButtons, Winapi.MMSystem, cxProgressBar, Vcl.ExtCtrls, uDM, cxListBox,
  cxLabel, cxCheckBox, System.ImageList, Vcl.ImgList, Vcl.ComCtrls, cxListView;

type
  TMainForm = class(TForm)
    dxSkinController: TdxSkinController;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    MainMenu: TMainMenu;
    File1: TMenuItem;
    Settings1: TMenuItem;
    About1: TMenuItem;
    About2: TMenuItem;
    English1: TMenuItem;
    Deutsch1: TMenuItem;
    cxGroupBox1: TcxGroupBox;
    cxMemo1: TcxMemo;
    cxButton1: TcxButton;
    cxGroupBox2: TcxGroupBox;
    cxMemo2: TcxMemo;
    cxButton2: TcxButton;
    cxProgressBar1: TcxProgressBar;
    Timer1: TTimer;
    cxGroupBox3: TcxGroupBox;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxGroupBox4: TcxGroupBox;
    cxMemo3: TcxMemo;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxMemo4: TcxMemo;
    cxGroupBox5: TcxGroupBox;
    cxMemo5: TcxMemo;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxMemo6: TcxMemo;
    cxMonsterListbox: TcxListBox;
    cxGroupBox6: TcxGroupBox;
    cxGroupBox7: TcxGroupBox;
    cxMIContainerMemo: TcxMemo;
    cxAdvancedGroupbox: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxTextEditID: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxTextEditName: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxTextEditAI: TcxTextEdit;
    cxGroupBox9: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxTextEditStr: TcxTextEdit;
    cxLabel5: TcxLabel;
    cxTextEditSta: TcxTextEdit;
    cxLabel6: TcxLabel;
    cxTextEditDex: TcxTextEdit;
    cxLabel7: TcxLabel;
    cxTextEditInt: TcxTextEdit;
    cxGroupBox10: TcxGroupBox;
    cxLabel8: TcxLabel;
    cxTextEditLevel: TcxTextEdit;
    cxLabel9: TcxLabel;
    cxTextEditRank: TcxTextEdit;
    cxLabel10: TcxLabel;
    cxTextEditMinAtk: TcxTextEdit;
    cxLabel11: TcxLabel;
    cxTextEditMaxAtk: TcxTextEdit;
    cxLabel12: TcxLabel;
    cxTextEditAtkSpeed: TcxTextEdit;
    cxLabel13: TcxLabel;
    cxTextEditAtkDelay: TcxTextEdit;
    cxLabel14: TcxLabel;
    cxTextEditHP: TcxTextEdit;
    cxLabel15: TcxLabel;
    cxTextEditMP: TcxTextEdit;
    cxLabel16: TcxLabel;
    cxTextEditDefense: TcxTextEdit;
    cxLabel17: TcxLabel;
    cxTextEditElement: TcxTextEdit;
    cxLabel18: TcxLabel;
    cxTextEditExp: TcxTextEdit;
    cxCheckBoxKillable: TcxCheckBox;
    cxLabel19: TcxLabel;
    cxTextEditIngameName: TcxTextEdit;
    cxEditAdvancedButton: TcxButton;
    ImageList: TImageList;
    cxSaveAdvanceButton: TcxButton;
    cxGroupBoxItemDrops: TcxGroupBox;
    cxGroupBox8: TcxGroupBox;
    cxListItemDropView: TcxGroupBox;
    cxTextEditSearch: TcxTextEdit;
    cxListBoxItem: TcxListBox;
    cxListItemDropGroupView: TcxListView;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxListView1: TcxListView;
    procedure Settings1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure English1Click(Sender: TObject);
    procedure Deutsch1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxMonsterListboxClick(Sender: TObject);
    procedure cxEditAdvancedButtonClick(Sender: TObject);
    procedure cxSaveAdvanceButtonClick(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
  private
    procedure loadLanguagefromIni(Languagecode: String);
    procedure InitSettingsIni;
    procedure ParseMonster(Filepath: String);
    procedure ParseItems(Filepath: String);
    procedure loadSettingsFromIni(Profile: String);
    procedure PlayWelcomeSound(Filename: String);
    function InitSQLite(Databasename: String): Boolean;
    function InsertMonsterIntoMasterDB(internalID,
      Monstername: String): Boolean;
    procedure ParseMonsterlist;
    function InsertItemsIntoMasterDB(internalID, Item: String): Boolean;
    procedure ParseItemlist;
    procedure ParseMonsterPropertys(Filepath: String);
    function ReplaceAllEvilChars(Str: String): String;
    function InsertMonsterPropertysIntoMasterDB(ParamStr: String): Boolean;
    procedure ParseItemPropertys(Filepath: String);
    function InsertItemPropertysIntoMasterDB(ParamStr: String): Boolean;
    function GetMonsterFromMasterDB: Integer;
    procedure GetMonsterPropertyFromMasterDB(szName: String);
    function ParsePropMoverEx(Filepath: String): String;
    function GetMIContainerSegment(MIContainerStr,
      TargetMISegment: String): String;
    function GetBetween(Str, StrStart, StrEnd: String): String;
    function GetToken(var S: string; const Separators: string; const Stop: string = string.Empty): string;
    function GetMonsterPropertyAdvancedFromMasterDB(szName, IngameName: String): Boolean;
    procedure ClearTMonsterPropertyAdvancedRecord;
    procedure FillAdvancedGroupbox;
    procedure UnlockAdvancedGroupbox;
    procedure LockAdvancedGroupbox;
    function GetAllDropKind(MIContainerStr: String): Boolean;
    function IsSubStringAt(const needle, haystack: string;
      position: Integer): Boolean;
    function GetAllDropItem(MIContainerStr: String): Boolean;
    function GetItemFromMasterDB: Integer;
    procedure FillListItemDropGroupView;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure InitLanguageIni;
  end;

type
  TMonsterPropertyAdvanced = record
    dwID: String;       //MI_AIBATT1
    szName: String;    //IDS_PROPMOVER_TXT_000006
    dwAI: String;      //AII_MONSTER
    dwIngameName: String;    //Aibatt
    dwStr: String;     //15  STR
    dwSta: String;     //20  STA
    dwDex: String;      //15 DEX
    dwInt: String;      //13
    dwLevel: String;    // Level des Monsters
    dwClass: String;    //Monster Rang   RANK_LOW f�r Small    RANK_NORMAL f�r normale Monster   RANK_CAPTAIN f�r Captains   RANK_BOSS f�r Giants und Boss Gegner   * RANK_SUPER f�r Monster wie CW. Diese Monster droppen unabh�ngig vom Level des Spielers
    dwAtkMin: String;   //Min Attacke
    dwAtkMax: String;   //Max Attacke
    dwAttackSpeed: String;
    dwReAttackDelay: String;
    dwAddHp: String;            //HP
    dwAddMp: String;            //MP
    dwNaturealArmor: String;    //DEF
    eElementType: String;
    dwCash: String;             //8 Penya?
    dwExpValue: String;         //EXP?
    bKillable: String; //Angreifbar / Killbar J/N
    MIContainer: String;
  end;

var
  MainForm: TMainForm;
  internalLanguageCode: String;
  MasterDB: String;
  sPropMonster, sPropMonsterProperty: String;
  sPropItem, sPropItemProperty: String;
  sMonsterSpawnProperty: String;

  MonsterList:         TStringList;
  ItemList:            TStringList;
  MonsterPropertyList: TStringList;
  ItemPropertyList:    TStringList;
  propMoverEx:         TStringList;


  dropKindList:        TStringList;
  dropItemList:        TStringList;


  MonsterPropertyAdvanced: TMonsterPropertyAdvanced;


  bAdvancedIsLocked: Boolean;

implementation

{$R *.dfm}

procedure TMainForm.cxButton10Click(Sender: TObject);
var
  i: Integer;
  s: String;
begin
  for i := 0 to ItemPropertyList.Count -1 do
    begin
      s := ReplaceAllEvilChars(ItemPropertyList[i]);
      InsertItemPropertysIntoMasterDB(s);

      {
      while Length(s) > 10 do
      begin
        cxMemo4.Lines.Add(GetToken(s, '|', '|'));
      end;
      cxMemo4.Lines.Add('#####');  }
    end;

end;

procedure TMainForm.cxButton11Click(Sender: TObject);
begin
  GetItemFromMasterDB;
end;

procedure TMainForm.cxButton12Click(Sender: TObject);
begin
  FillListItemDropGroupView;
end;

procedure TMainForm.cxEditAdvancedButtonClick(Sender: TObject);
begin
  if bAdvancedIsLocked then
  begin
    UnlockAdvancedGroupbox;
    cxEditAdvancedButton.ImageIndex := 1;
    cxEditAdvancedButton.Caption := 'Lock'
  end else
  begin
    LockAdvancedGroupbox;
    cxEditAdvancedButton.ImageIndex := 0;
    cxEditAdvancedButton.Caption := 'Unlock'
  end;

end;

procedure TMainForm.cxSaveAdvanceButtonClick(Sender: TObject);
begin
 //Save
end;

function TMainForm.GetMonsterFromMasterDB: Integer;
var
  Monstername: String;
begin
  DM.qMonster.SQL.Text := '';
  DM.qMonster.SQL.Text := 'SELECT * FROM FRE_Monster';
  DM.qMonster.Open;

  try
    cxMonsterListbox.Items.Clear;

    while not DM.qMonster.Eof do
    begin
      Monstername := DM.qMonster.Fields[2].AsString;
      cxMonsterListbox.Items.Add(Monstername);
      DM.qMonster.Next;
    end;

  finally
    Result := DM.qMonster.RecordCount;
    DM.qMonster.Close;
  end;
end;

procedure TMainForm.GetMonsterPropertyFromMasterDB(szName: String);
var
  dwID: String;
begin
  DM.qMonsterProperty.Close;
  DM.qMonsterProperty.SQL.Clear;
  DM.qMonsterProperty.SQL.Add('SELECT id, dwID, szName FROM FRE_MonsterProperty');
  DM.qMonsterProperty.SQL.Add('WHERE szName = :szName');

  DM.qMonsterProperty.ParamByName('szName').AsString := szName;
  DM.qMonsterProperty.Open;
  try
    while not DM.qMonsterProperty.Eof do
    begin
      dwID := DM.qMonsterProperty.Fields[1].AsString;
     // cxMonsterPropertyListbox.Items.Add(dwID);
      DM.qMonsterProperty.Next;
    end;

    //ShowMessage(DM.qMonsterProperty.FieldByName('dwID').AsString);
  finally
    DM.qMonsterProperty.Close;
  end;
end;

function TMainForm.GetMonsterPropertyAdvancedFromMasterDB(szName, IngameName: String): Boolean;
begin
  ClearTMonsterPropertyAdvancedRecord;
  DM.qMonsterPropertyAdvanced.Close;
  DM.qMonsterPropertyAdvanced.SQL.Clear;
  DM.qMonsterPropertyAdvanced.SQL.Add('SELECT * FROM FRE_MonsterProperty');
  DM.qMonsterPropertyAdvanced.SQL.Add('WHERE szName = :szName');

  DM.qMonsterPropertyAdvanced.ParamByName('szName').AsString := szName;
  DM.qMonsterPropertyAdvanced.Open;
  try
    while not DM.qMonsterPropertyAdvanced.Eof do
    begin
      MonsterPropertyAdvanced.dwID              := DM.qMonsterPropertyAdvanced.FieldByName('dwID').AsString;
      MonsterPropertyAdvanced.szName            := DM.qMonsterPropertyAdvanced.FieldByName('szName').AsString;
      MonsterPropertyAdvanced.dwAI              := DM.qMonsterPropertyAdvanced.FieldByName('dwAI').AsString;
      MonsterPropertyAdvanced.dwIngameName      :=     IngameName;
      MonsterPropertyAdvanced.dwStr             := DM.qMonsterPropertyAdvanced.FieldByName('dwStr').AsString;
      MonsterPropertyAdvanced.dwSta := DM.qMonsterPropertyAdvanced.FieldByName('dwSta').AsString;
      MonsterPropertyAdvanced.dwDex := DM.qMonsterPropertyAdvanced.FieldByName('dwDex').AsString;
      MonsterPropertyAdvanced.dwInt := DM.qMonsterPropertyAdvanced.FieldByName('dwInt').AsString;
      MonsterPropertyAdvanced.dwLevel := DM.qMonsterPropertyAdvanced.FieldByName('dwLevel').AsString;
      MonsterPropertyAdvanced.dwClass := DM.qMonsterPropertyAdvanced.FieldByName('dwClass').AsString;
      MonsterPropertyAdvanced.dwAtkMin := DM.qMonsterPropertyAdvanced.FieldByName('dwAtkMin').AsString;
      MonsterPropertyAdvanced.dwAtkMax := DM.qMonsterPropertyAdvanced.FieldByName('dwAtkMax').AsString;
      MonsterPropertyAdvanced.dwAttackSpeed := DM.qMonsterPropertyAdvanced.FieldByName('dwAttackSpeed').AsString;
      MonsterPropertyAdvanced.dwReAttackDelay := DM.qMonsterPropertyAdvanced.FieldByName('dwReAttackDelay').AsString;
      MonsterPropertyAdvanced.dwAddHp := DM.qMonsterPropertyAdvanced.FieldByName('dwAddHp').AsString;
      MonsterPropertyAdvanced.dwAddMp := DM.qMonsterPropertyAdvanced.FieldByName('dwAddMp').AsString;
      MonsterPropertyAdvanced.dwNaturealArmor := DM.qMonsterPropertyAdvanced.FieldByName('dwNaturealArmor').AsString;
      MonsterPropertyAdvanced.eElementType := DM.qMonsterPropertyAdvanced.FieldByName('eElementType').AsString;
      MonsterPropertyAdvanced.dwCash := DM.qMonsterPropertyAdvanced.FieldByName('dwCash').AsString;
      MonsterPropertyAdvanced.dwExpValue := DM.qMonsterPropertyAdvanced.FieldByName('dwExpValue').AsString;
      MonsterPropertyAdvanced.bKillable := DM.qMonsterPropertyAdvanced.FieldByName('bKillable').AsString;
      DM.qMonsterPropertyAdvanced.Next;
    end;

    //ShowMessage(DM.qMonsterProperty.FieldByName('dwID').AsString);
  finally
    if DM.qMonsterPropertyAdvanced.RecordCount = 0
    then begin
      Result := False;
    end else begin
      Result := True;
    end;

    DM.qMonsterPropertyAdvanced.Close;
  end;
end;

procedure TMainForm.cxButton1Click(Sender: TObject);
begin
  ParseMonster(sPropMonster);
end;

procedure TMainForm.cxButton2Click(Sender: TObject);
begin
  ParseItems(sPropItem);
end;

procedure TMainForm.cxButton3Click(Sender: TObject);
begin
  ParseMonsterlist;
end;

procedure TMainForm.cxButton4Click(Sender: TObject);
begin
  ParseItemlist;
end;

procedure TMainForm.cxButton5Click(Sender: TObject);
var
  i: Integer;
  s: String;
  internal_id, monstername: String;
begin
  for i := 0 to MonsterList.Count -1 do
    begin
      s := MonsterList[i];
      s := ReplaceAllEvilChars(s);
      internal_id := GetToken(s, '|', '|');
      monstername := GetToken(s, '|', '|');

      InsertMonsterIntoMasterDB(internal_id, monstername);
      //ShowMessage(internal_id + ' ' + monstername);
  end;
end;

procedure TMainForm.cxButton6Click(Sender: TObject);
var
  i, sLength: Integer;
  s: String;
  internal_id, Item: String;
begin
  for i := 0 to ItemList.Count -1 do
    begin
      s := ItemList[i];
      sLength := Length(s);
      internal_id := Copy(ItemList[i], 1, 24);
      Item := Copy(ItemList[i], 25, sLength - 24);
      //InsertMonsterIntoMasterDB(internal_id, monstername);
      ShowMessage(internal_id + ' ' + Item);
  end;
end;



procedure TMainForm.cxButton7Click(Sender: TObject);
begin
  ParseMonsterPropertys(sPropMonsterProperty);
end;




procedure TMainForm.cxButton8Click(Sender: TObject);
var
  i: Integer;
  s: String;
begin
  for i := 0 to MonsterPropertyList.Count -1 do
    begin
      s := ReplaceAllEvilChars(MonsterPropertyList[i]);
      InsertMonsterPropertysIntoMasterDB(s);

      {
      while Length(s) > 10 do
      begin
        cxMemo4.Lines.Add(GetToken(s, '|', '|'));
      end;
      cxMemo4.Lines.Add('#####');  }
    end;

end;

procedure TMainForm.cxButton9Click(Sender: TObject);
begin
  ParseItemPropertys(sPropItemProperty);
end;

  //  SelectedItem := cxListBox.items[cxListBox.itemindex];
  //DM.qMonster.Fields[0].AsString)
procedure TMainForm.cxMonsterListboxClick(Sender: TObject);
var
  sMonsterSearch: string;
begin
  if cxMonsterListbox.ItemIndex <> -1 then
  begin
    DM.qMonster.Close;
    DM.qMonster.SQL.Clear;
    DM.qMonster.SQL.Add('SELECT id, internal_id, Monstername FROM FRE_Monster');
    DM.qMonster.SQL.Add('WHERE Monstername = :monstername');
    sMonsterSearch := cxMonsterListbox.Items[cxMonsterListbox.ItemIndex];
    DM.qMonster.ParamByName('monstername').AsString := sMonsterSearch;
    DM.qMonster.Open;
    try
    //  ShowMessage(DM.qMonster.FieldByName('internal_id').AsString);
      //GetMonsterPropertyFromMasterDB(DM.qMonster.FieldByName('internal_id').AsString);
      if GetMonsterPropertyAdvancedFromMasterDB(DM.qMonster.FieldByName('internal_id').AsString, sMonsterSearch)
        then begin
          FillAdvancedGroupbox;
          cxMIContainerMemo.Text     := GetMIContainerSegment(ParsePropMoverEx(sMonsterSpawnProperty), MonsterPropertyAdvanced.dwID);
          cxAdvancedGroupbox.Caption := MonsterPropertyAdvanced.dwIngameName + ' - ' + 'Level ' + MonsterPropertyAdvanced.dwLevel;
          GetAllDropKind(MonsterPropertyAdvanced.MIContainer);
          GetAllDropItem(MonsterPropertyAdvanced.MIContainer);
        end else begin
          ShowMessage('Something went wrong!' + ' ' + DM.qMonster.SQL.Text);
        end;

    finally
      DM.qMonster.Close;
    end;
  end;
end;

procedure TMainForm.Deutsch1Click(Sender: TObject);
begin
  loadLanguagefromIni('DE');
end;

procedure TMainForm.English1Click(Sender: TObject);
begin
  loadLanguagefromIni('EN');
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  MasterDB := 'Master.db'; //SQL Lite Database

  MonsterList         := TStringList.Create;
  ItemList            := TStringList.Create;
  MonsterPropertyList := TStringList.Create;
  ItemPropertyList    := TStringList.Create;
  propMoverEx         := TStringList.Create;
  dropKindList        := TStringList.Create;
  dropItemList        := TStringList.Create;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
 // DM.UniQuery.Close;
  //DM.UniConnection.Close;

  MonsterList.Free;
  ItemList.Free;
  MonsterPropertyList.Free;
  ItemPropertyList.Free;
  propMoverEx.Free;
  dropKindList.Free;
  dropItemList.Free;

  DM.qMonster.Close;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  InitLanguageIni;    // function true / false TODO**
  InitSettingsIni;   // function true / false TODO**
  InitSQLite(MasterDB);    // function true / false TODO**
  LockAdvancedGroupbox;

  ClearTMonsterPropertyAdvancedRecord;

  //DB Verbindungscheck TODO**
//File Exist check auf die Dateien

  cxGroupBox6.Caption := IntToStr(GetMonsterFromMasterDB) + ' available Monster';
  PlayWelcomeSound('Welcome.mp3');

  cxMonsterListbox.ItemIndex := 6;
  cxMonsterListboxClick(self);
end;


procedure TMainForm.InitLanguageIni;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\languages.ini');
  internalLanguageCode := 'EN';

  if not FileExists(ExtractFilePath(ParamStr(0)) + '\languages.ini') then //Wenns nicht existiert ...
  begin
    //Standarteinstellung
    Ini.WriteString('EN', 'Caption', 'FRE ~ [Flyff Ressource Editor]');
    Ini.WriteString('EN', 'Item0', 'File');
    Ini.WriteString('EN', 'Item1', 'Settings');
    Ini.WriteString('EN', 'Item2', 'Language');
    Ini.WriteString('EN', 'Item3', 'About');

    Ini.WriteString('EN', 'PageCaption1', 'Welcome');
    Ini.WriteString('EN', 'PageCaption2', 'Data Overview');
    Ini.WriteString('EN', 'PageCaption3', 'Monster Item Droprate');



    Ini.WriteString('DE', 'Caption', 'FRE ~ [Flyff Ressource Editor]');
    Ini.WriteString('DE', 'Item0', 'Datei');
    Ini.WriteString('DE', 'Item1', 'Einstellungen');
    Ini.WriteString('DE', 'Item2', 'Sprache');
    Ini.WriteString('DE', 'Item3', '�ber');

    Ini.WriteString('DE', 'PageCaption1', 'Willkommen');
    Ini.WriteString('DE', 'PageCaption2', 'Daten �bersicht');
    Ini.WriteString('DE', 'PageCaption3', 'Monster Item Droprate');



    Ini.Free;
  //  cxApiKeyEdit.Text  := Ini.ReadString('General', 'ApiKey', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  end
  else
  begin    //Wenn es schon existiert
   // Apikey  := Ini.ReadString('General', 'ApiKey', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  //  cxApiKeyEdit.Text := Apikey;

   // URL := Ini.ReadString('General', 'Licenseserver', '');

   // cxLicenseEdit.Text := RemoveSlashFromURL(URL);
  end;
end;

procedure TMainForm.loadLanguagefromIni(Languagecode: String);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\languages.ini');
  internalLanguageCode := Languagecode;

  if Languagecode = 'EN'
    then begin
      MainForm.Caption          := Ini.ReadString(Languagecode, 'Caption', '');
      MainMenu.Items[0].Caption := Ini.ReadString(Languagecode, 'Item0', '');
      MainMenu.Items[1].Caption := Ini.ReadString(Languagecode, 'Item1', '');
      MainMenu.Items[2].Caption := Ini.ReadString(Languagecode, 'Item2', '');
      MainMenu.Items[3].Caption := Ini.ReadString(Languagecode, 'Item3', '');

      cxPageControl1.Pages[0].Caption := Ini.ReadString(Languagecode, 'PageCaption1', '');
      cxPageControl1.Pages[1].Caption := Ini.ReadString(Languagecode, 'PageCaption2', '');
      cxPageControl1.Pages[2].Caption := Ini.ReadString(Languagecode, 'PageCaption3', '');
    end;

  if Languagecode = 'DE'
    then begin
      MainForm.Caption          := Ini.ReadString(Languagecode, 'Caption', '');
      MainMenu.Items[0].Caption := Ini.ReadString(Languagecode, 'Item0', '');
      MainMenu.Items[1].Caption := Ini.ReadString(Languagecode, 'Item1', '');
      MainMenu.Items[2].Caption := Ini.ReadString(Languagecode, 'Item2', '');
      MainMenu.Items[3].Caption := Ini.ReadString(Languagecode, 'Item3', '');


      cxPageControl1.Pages[0].Caption := Ini.ReadString(Languagecode, 'PageCaption1', '');
      cxPageControl1.Pages[1].Caption := Ini.ReadString(Languagecode, 'PageCaption2', '');
      cxPageControl1.Pages[2].Caption := Ini.ReadString(Languagecode, 'PageCaption3', '');
    end;

  Ini.Free;
  end;

procedure TMainForm.Settings1Click(Sender: TObject);
begin
  SettingsForm.ShowModal;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
var
  Ini: TIniFile;
  s: String;
begin
  if FileExists(ExtractFilePath(ParamStr(0)) + '\settings.ini') then begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\settings.ini');
  s := Ini.ReadString('Profile 1', 'propMover.txt.txt', '');
  if s <> '' then
    begin
      cxProgressBar1.Position := 25;
      ParseMonster(sPropMonster);
      cxProgressBar1.Position := 50;
      ParseItems(sPropItem);
      cxProgressBar1.Position := 75;
      Timer1.Enabled := False;
      cxProgressBar1.Position := 100;
    end;
    Ini.Free;
  end;
end;

procedure TMainForm.InitSettingsIni;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\settings.ini');

  if not FileExists(ExtractFilePath(ParamStr(0)) + '\settings.ini') then //Wenns nicht existiert ...
  begin
    //Standarteinstellung
    Ini.WriteString('Profile 1', 'propMover.txt.txt', '');
    Ini.WriteString('Profile 1', 'propMover.txt', '');
    Ini.WriteString('Profile 1', 'propMoverEx.inc', '');
    Ini.WriteString('Profile 1', 'probItem.txt.txt', '');
    Ini.WriteString('Profile 1', 'propItem.txt', '');



    Ini.Free;
  end
  else
  begin
    loadSettingsFromIni('Profile 1');
  end;
end;

procedure TMainForm.ParseMonster(Filepath: String);
var
  TargetList: TStringList;
  i: Integer;
  s, firstStr, SecondStr: String;

begin
  TargetList := TStringList.Create;
  TargetList.LoadFromFile(Filepath);

  for i := TargetList.Count -1 downto 0 do
    begin

      s := TargetList[i];
      s := ReplaceAllEvilChars(s);

      firstStr :=  GetToken(s, '|', '|');
      SecondStr := GetToken(s, '|', '|');

      if (SecondStr = '') or (SecondStr = ' ') then begin
        TargetList.Delete(i);
      end;

    end;

  MonsterList.Assign(TargetList);
  cxMemo1.Lines := TargetList;
  cxGroupBox1.Caption := IntToStr(TargetList.Count) +  ' available Monster';
  TargetList.Free;
end;

procedure TMainForm.ParseItems(Filepath: String);
var
  TargetList: TStringList;
  i: Integer;
  s: String;

begin
  TargetList := TStringList.Create;
  TargetList.LoadFromFile(Filepath);

  for i := TargetList.Count -1 downto 0 do
    begin
      s := Copy(TargetList[i], 26, 1);
      if s = '' then begin
        TargetList.Delete(i);
      end;
    end;

  ItemList.Assign(TargetList);                         // ItemList = TStringList f�r die DB
  cxMemo2.Lines := TargetList;
  cxGroupBox2.Caption := IntToStr(TargetList.Count) +  ' available Items';
  TargetList.Free;
end;

procedure TMainForm.ParseMonsterPropertys(Filepath: String);
var
  TargetList: TStringList;
  i: Integer;
  s: String;

begin
  TargetList := TStringList.Create;
  TargetList.LoadFromFile(Filepath);

  for i := TargetList.Count -1 downto 0 do
    begin
      s := ReplaceAllEvilChars(TargetList[i]);
      s := Copy(TargetList[i], 1, 1);

      if (s = '') or (s = '/') then
      begin
        TargetList.Delete(i);
      end;

    end;

  MonsterPropertyList.Assign(TargetList);                         // MonsterPropertyList = TStringList f�r die DB
  cxMemo3.Lines := TargetList;
  cxGroupBox4.Caption := IntToStr(TargetList.Count) +  ' available Monsterpropertys';
  TargetList.Free;
end;

procedure TMainForm.loadSettingsFromIni(Profile: String);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\settings.ini');

  if Profile = 'Profile 1'
    then begin
      sPropMonster := Ini.ReadString(Profile, 'propMover.txt.txt', '');
      sPropMonsterProperty := Ini.ReadString(Profile, 'propMover.txt', '');
      sMonsterSpawnProperty := Ini.ReadString(Profile, 'propMoverEx.inc', '');
      sPropItem := Ini.ReadString(Profile, 'probItem.txt.txt', '');
      sPropItemProperty := Ini.ReadString(Profile, 'propItem.txt', '');
    end;

  Ini.Free;
end;

procedure TMainForm.PlayWelcomeSound(Filename: String);
begin
  mciSendString('open welcome.mp3 type mpegvideo alias song1', nil, 0, 0);
  mciSendString('play song1', nil, 0, 0);
end;

function TMainForm.InitSQLite(Databasename: String): Boolean;
begin
  DM.UniConnection.ProviderName := 'SQLite';
  DM.UniConnection.Database     := Databasename;
  DM.qMasterDBTransfer.Connection := DM.UniConnection;


 // ShowMessage(DM.UniQuery.FieldByName('Monstername').AsString);
  Result := True;
end;

function TMainForm.InsertMonsterIntoMasterDB(internalID, Monstername: String): Boolean;
begin
  DM.qMasterDBTransfer.SQL.Text := '';
  DM.qMasterDBTransfer.SQL.Text := 'SELECT * FROM FRE_Monster WHERE internal_id = :internal_id';
  DM.qMasterDBTransfer.ParamByName('internal_id').AsString := internalID;
  DM.qMasterDBTransfer.Open;

  if (DM.qMasterDBTransfer.RecordCount = 0) AND (internalID <> '') AND (Monstername <> '')  then
  begin
    DM.qMasterDBTransfer.SQL.Clear;
    DM.qMasterDBTransfer.SQL.Add('INSERT INTO FRE_Monster(internal_id, Monstername)');
    DM.qMasterDBTransfer.SQL.Add('VALUES(:internal_id, :Monstername)');

    DM.qMasterDBTransfer.ParamByName('internal_id').AsString  := internalID;
    DM.qMasterDBTransfer.ParamByName('Monstername').AsString   := Monstername;
    DM.qMasterDBTransfer.ExecSQL;
  end
  else
  begin
     { DM.UniQuery.SQL.Text := '';
    DM.UniQuery.SQL.Text := 'UPDATE FRE_Monster SET Count = Count + 1 WHERE Word = :word';
    DM.UniQuery.ParamByName('word').AsString := tmpWord;
    DM.UniQuery.ExecSQL;     }
  end;
  Result := True;
end;

procedure TMainForm.ParseMonsterlist;
var
  i, sLength: Integer;
  s: String;
  internal_id, monstername: String;
begin
  for i := 0 to MonsterList.Count -1 do
    begin
      s := MonsterList[i];
      sLength := Length(s);
      internal_id := Copy(MonsterList[i], 1, 25);
      monstername := Copy(MonsterList[i], 26, sLength - 25);
      InsertMonsterIntoMasterDB(internal_id, monstername);
    end;
end;

function TMainForm.InsertItemsIntoMasterDB(internalID, Item: String): Boolean;
begin
  DM.qMasterDBTransfer.SQL.Text := '';
  DM.qMasterDBTransfer.SQL.Text := 'SELECT * FROM FRE_Item WHERE internal_id_item = :internal_id_item';
  DM.qMasterDBTransfer.ParamByName('internal_id_item').AsString := internalID;
  DM.qMasterDBTransfer.Open;

  if DM.qMasterDBTransfer.RecordCount = 0 then
  begin
    DM.qMasterDBTransfer.SQL.Clear;
    DM.qMasterDBTransfer.SQL.Add('INSERT INTO FRE_Item(internal_id_item, item_name)');
    DM.qMasterDBTransfer.SQL.Add('VALUES(:internal_id_item, :item_name)');

    DM.qMasterDBTransfer.ParamByName('internal_id_item').AsString  := internalID;
    DM.qMasterDBTransfer.ParamByName('item_name').AsString   := Item;
    DM.qMasterDBTransfer.ExecSQL;
  end
  else
  begin
     { DM.UniQuery.SQL.Text := '';
    DM.UniQuery.SQL.Text := 'UPDATE FRE_Monster SET Count = Count + 1 WHERE Word = :word';
    DM.UniQuery.ParamByName('word').AsString := tmpWord;
    DM.UniQuery.ExecSQL;     }
  end;
  Result := True;
end;

procedure TMainForm.ParseItemlist;
var
  i, sLength: Integer;
  s: String;
  internal_id, Item: String;
begin
  for i := 0 to ItemList.Count -1 do
    begin
      s := ItemList[i];
      sLength := Length(s);
      internal_id := Copy(ItemList[i], 1, 24);
      Item := Copy(ItemList[i], 25, sLength - 24);
      InsertItemsIntoMasterDB(internal_id, Item);
    end;
end;

function TMainForm.ReplaceAllEvilChars(Str: String): String;
var
  tmp: String;
begin
  tmp := StringReplace(Str, Char(#9), '|', [rfReplaceAll, rfIgnoreCase]);
 {
  tmp := StringReplace(tmp, '.', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, ',', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, ';', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, ':', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, '-', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, '!', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, '?', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, '(', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, ')', '', [rfReplaceAll, rfIgnoreCase]);
  tmp := StringReplace(tmp, '''', '', [rfReplaceAll, rfIgnoreCase]); }
  Result := tmp;
end;

function TMainForm.InsertMonsterPropertysIntoMasterDB(ParamStr: String): Boolean;
var
  Backup, dwID: String;
begin
  Backup := Backup + Paramstr;
  dwID := GetToken(Backup, '|', '|');

  DM.qMasterDBTransfer.SQL.Text := '';
  DM.qMasterDBTransfer.SQL.Text := 'SELECT * FROM FRE_MonsterProperty WHERE dwID = :dwID';
  DM.qMasterDBTransfer.ParamByName('dwID').AsString := dwID;
  DM.qMasterDBTransfer.Open;

  if DM.qMasterDBTransfer.RecordCount = 0 then
  begin
    DM.qMasterDBTransfer.SQL.Clear;
    DM.qMasterDBTransfer.SQL.Add('INSERT INTO FRE_MonsterProperty(dwID,	szName,	dwAI,	dwStr,	dwSta,	dwDex,	dwInt,'+
    ' dwHR,	dwER,	dwRace,	dwBelligerence,	dwGender,	dwLevel,	dwFilghtLevel,	dwSize,	dwClass,	bIfPart,'+
    '	dwKarma,	dwUseable,	dwActionRadius,	dwAtkMin,	dwAtkMax,	dwAtk1,	dwAtk2,	dwAtk3,	dwHorizontalRate,'+
    '	dwVerticalRate,	dwDiagonalRate,	dwThrustRate,	dwChestRate,	dwHeadRate,	dwArmRate,	dwLegRate,	dwAttackSpeed,'+
    '	dwReAttackDelay,	dwAddHp,	dwAddMp,	dwNaturealArmor,	nAbrasion,	nHardness,	dwAdjAtkDelay,	eElementType,'+
    '	wElementAtk,	dwHideLevel,	fSpeed,	dwShelter,	bFlying,	dwJumpIng,	dwAirJump,	bTaming,	dwResisMagic,'+
    '	fResistElecricity,	fResistFire,	fResistWind,	fResistWater,	fResistEarth,	dwCash,	dwSourceMaterial,'+
    '	dwMaterialAmount,	dwCohesion,	dwHoldingTime,	dwCorrectionValue,	dwExpValue,	nFxpValue,	nBodyState,'+
    '	dwAddAbility,	bKillable,	dwVirtItem1,	dwVirtType1,	dwVirtItem2,	dwVirtType2,	dwVirtItem3,	dwVirtType3,'+
    '	dwSndAtk1,	dwSndAtk2,	dwSndDie1,	dwSndDie2,	dwSndDmg1,	dwSndDmg2,	dwSndDmg3,	dwSndIdle1,	dwSndIdle2,	szComment)');

    DM.qMasterDBTransfer.SQL.Add('VALUES(:dwID,	:szName,	:dwAI,	:dwStr,	:dwSta,	:dwDex,	:dwInt,'+
    ' :dwHR,	:dwER,	:dwRace,	:dwBelligerence,	:dwGender,	:dwLevel,	:dwFilghtLevel,	:dwSize,	:dwClass,	:bIfPart,'+
    '	:dwKarma,	:dwUseable,	:dwActionRadius,	:dwAtkMin,	:dwAtkMax,	:dwAtk1,	:dwAtk2,	:dwAtk3,	:dwHorizontalRate,'+
    '	:dwVerticalRate,	:dwDiagonalRate,	:dwThrustRate,	:dwChestRate,	:dwHeadRate,	:dwArmRate,	:dwLegRate,	:dwAttackSpeed,'+
    '	:dwReAttackDelay,	:dwAddHp,	:dwAddMp,	:dwNaturealArmor,	:nAbrasion,	:nHardness,	:dwAdjAtkDelay,	:eElementType,'+
    '	:wElementAtk,	:dwHideLevel,	:fSpeed,	:dwShelter,	:bFlying,	:dwJumpIng,	:dwAirJump,	:bTaming,	:dwResisMagic,'+
    '	:fResistElecricity,	:fResistFire,	:fResistWind,	:fResistWater,	:fResistEarth,	:dwCash,	:dwSourceMaterial,'+
    '	:dwMaterialAmount,	:dwCohesion,	:dwHoldingTime,	:dwCorrectionValue,	:dwExpValue,	:nFxpValue,	:nBodyState,'+
    '	:dwAddAbility,	:bKillable,	:dwVirtItem1,	:dwVirtType1,	:dwVirtItem2,	:dwVirtType2,	:dwVirtItem3,	:dwVirtType3,'+
    '	:dwSndAtk1,	:dwSndAtk2,	:dwSndDie1,	:dwSndDie2,	:dwSndDmg1,	:dwSndDmg2,	:dwSndDmg3,	:dwSndIdle1,	:dwSndIdle2,	:szComment)');

    DM.qMasterDBTransfer.ParamByName('dwID').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('szName').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAI').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwStr').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSta').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwDex').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwInt').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwHR').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwER').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwRace').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwBelligerence').AsString     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwGender').AsString           :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwLevel').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwFilghtLevel').AsString      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSize').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwClass').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bIfPart').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwKarma').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwUseable').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwActionRadius').AsString     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAtkMin').AsString           :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAtkMax').AsString           :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAtk1').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAtk2').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAtk3').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwHorizontalRate').AsString   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVerticalRate').AsString     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwDiagonalRate').AsString     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwThrustRate').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwChestRate').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwHeadRate').AsString         :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwArmRate').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwLegRate').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAttackSpeed').AsString      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReAttackDelay').AsString    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAddHp').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAddMp').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwNaturealArmor').AsString    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nAbrasion').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nHardness').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAdjAtkDelay').AsString      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('eElementType').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('wElementAtk').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwHideLevel').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fSpeed').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwShelter').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bFlying').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwJumpIng').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAirJump').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bTaming').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwResisMagic').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fResistElecricity').AsString  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fResistFire').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fResistWind').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fResistWater').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fResistEarth').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwCash').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSourceMaterial').AsString   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwMaterialAmount').AsString   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwCohesion').AsString         :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwHoldingTime').AsString      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwCorrectionValue').AsString  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwExpValue').AsString         :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nFxpValue').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nBodyState').AsString         :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAddAbility').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bKillable').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVirtItem1').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVirtType1').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVirtItem2').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVirtType2').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVirtItem3').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwVirtType3').AsString        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndAtk1').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndAtk2').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndDie1').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndDie2').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndDmg1').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndDmg2').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndDmg3').AsString          :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndIdle1').AsString         :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndIdle2').AsString         :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('szComment').AsString          :=   GetToken(ParamStr, '|', '|');

    DM.qMasterDBTransfer.ExecSQL;
  end
  else
  begin
     { DM.qMasterDBTransfer.SQL.Text := '';
    DM.qMasterDBTransfer.SQL.Text := 'UPDATE FRE_Monster SET Count = Count + 1 WHERE Word = :word';
    DM.qMasterDBTransfer.ParamByName('word').AsString := tmpWord;
    DM.qMasterDBTransfer.ExecSQL;     }
  end;
  Result := True;
end;


procedure TMainForm.ParseItemPropertys(Filepath: String);
var
  TargetList: TStringList;
  i: Integer;
  s: String;

begin
  TargetList := TStringList.Create;
  TargetList.LoadFromFile(Filepath);

  for i := TargetList.Count -1 downto 0 do
    begin
      s := ReplaceAllEvilChars(TargetList[i]);
      s := Copy(TargetList[i], 1, 1);

      if (s = '') or (s = '/') then
      begin
        TargetList.Delete(i);
      end;

      {
      //Tokenauslesen
      while Length(BackupStr) > 10 do
      begin
        cxMemo4.Lines.Add(GetToken(BackupStr, '|', '|'));
      end;

      cxMemo4.Lines.Add('#####'); }


    end;

  ItemPropertyList.Assign(TargetList);                         // ItemPropertyList = TStringList f�r die DB
  cxMemo5.Lines := TargetList;
  cxGroupBox5.Caption := IntToStr(TargetList.Count) +  ' available Itempropertys';
  TargetList.Free;
end;

function TMainForm.InsertItemPropertysIntoMasterDB(ParamStr: String): Boolean;
var
  Backup, dwID, ver6, szName: String;
begin
  Backup := Backup + Paramstr;
  ver6   := GetToken(Backup, '|', '|'); //ver6
  dwID   := GetToken(Backup, '|', '|'); //dwid
  szName := GetToken(Backup, '|', '|'); //szName

  DM.qMasterDBTransfer.SQL.Text := '';
  DM.qMasterDBTransfer.SQL.Text := 'SELECT * FROM FRE_ItemProperty WHERE dwID = :dwID';
  DM.qMasterDBTransfer.ParamByName('dwID').AsString := dwID;
  DM.qMasterDBTransfer.Open;

  if DM.qMasterDBTransfer.RecordCount = 0 then
  begin
    DM.qMasterDBTransfer.SQL.Clear;
    DM.qMasterDBTransfer.SQL.Add('INSERT INTO FRE_ItemProperty(ver6, dwID,	szName,	dwNum,	dwPackMax,	dwItemKind1,	dwItemKind2,	dwItemKind3,'+
    ' dwItemJob,	bPermanence,	dwUseable,	dwItemSex,	dwCost,	dwEndurance,	nAbrasion,	nMaxRepair,	dwHanded,	dwFlag,'+
    '	dwParts,	dwPartsub,	bPartFile,	dwExclusive,	dwBasePartsIgnore,	dwItemLV,	dwItemRare,	dwShopAble,	bLog,'+
    '	bCharged,	dwLinkKindBullet,	dwLinkKind,	dwAbilityMin,	dwAbilityMax,	eItemType,	wItemEAtk,	dwParry,'+
    '	dwBlockRating,	dwAddSkillMin,	dwAddSkillMax,	dwAtkStyle,	dwWeaponType,	dwItemAtkOrder1,	dwItemAtkOrder2,	dwItemAtkOrder3,'+
    '	dwItemAtkOrder4,	bContinuousPain,	dwShellQuantity,	dwRecoil,	dwLoadingTime,	nAdjHitRate,	dwAttackSpeed,	dwDmgShift,	dwAttackRange,'+
    '	dwProbability,	dwDestParam1,	dwDestParam2,	dwDestParam3,	nAdjParamVal1,	nAdjParamVal2,	nAdjParamVal3,'+
    '	dwChgParamVal1,	dwChgParamVal2,	dwChgParamVal3,	dwdestData1,	dwdestData2,	dwdestData3,	dwactiveskill,'+
    '	dwactiveskillLv,	dwactiveskillper,	dwReqMp,	dwRepFp,	dwReqDisLV,	dwReSkill1,	dwReSkillLevel1,	dwReSkill2,'+
    '	dwReSkillLevel2,	dwSkillReadyType,	dwSkillReady,	dwSkillRange,	dwSfxElemental,	dwSfxObj,	dwSfxObj2,	dwSfxObj3,'+
    '	dwSfxObj4,	dwSfxObj5,	dwUseMotion,	dwCircleTime,	dwSkillTime,	dwExeTarget,	dwUseChance,	dwSpellRegion,'+
    '	dwSpellType,	dwReferStat1,	dwReferStat2,	dwReferTarget1,	dwReferTarget2,	dwReferValue1,	dwReferValue2,	dwSkillType,'+
    '	fItemResistElecricity,	fItemResistFire,	fItemResistWind,	fItemResistWater,	fItemResistEarth,	nEvildoing,	dwExpertLV,	ExpertMax,'+
    '	dwSubDefine,	dwExp,	dwComboStyle,	fFlightSpeed,	fFlightLRAngle,	fFlightTBAngle,	dwFlightLimit,	dwFFuelReMax,'+
    '	dwAFuelReMax,	dwFuelRe,	dwLimitLevel1,	dwReflect,	dwSndAttack1,	dwSndAttack2,	szIcon,	dwQuestID,'+
    '	szTextFile,	szComment)');

    DM.qMasterDBTransfer.SQL.Add('VALUES(:ver6, :dwID,	:szName,	:dwNum,	:dwPackMax,	:dwItemKind1,	:dwItemKind2,	:dwItemKind3,'+
    ' :dwItemJob,	:bPermanence,	:dwUseable,	:dwItemSex,	:dwCost,	:dwEndurance,	:nAbrasion,	:nMaxRepair,	:dwHanded,	:dwFlag,'+
    '	:dwParts,	:dwPartsub,	:bPartFile,	:dwExclusive,	:dwBasePartsIgnore,	:dwItemLV,	:dwItemRare,	:dwShopAble,	:bLog,'+
    '	:bCharged,	:dwLinkKindBullet,	:dwLinkKind,	:dwAbilityMin,	:dwAbilityMax,	:eItemType,	:wItemEAtk,	:dwParry,'+
    '	:dwBlockRating,	:dwAddSkillMin,	:dwAddSkillMax,	:dwAtkStyle,	:dwWeaponType,	:dwItemAtkOrder1,	:dwItemAtkOrder2,	:dwItemAtkOrder3,'+
    '	:dwItemAtkOrder4,	:bContinuousPain,	:dwShellQuantity,	:dwRecoil,	:dwLoadingTime,	:nAdjHitRate,	:dwAttackSpeed,	:dwDmgShift,	:dwAttackRange,'+
    '	:dwProbability,	:dwDestParam1,	:dwDestParam2,	:dwDestParam3,	:nAdjParamVal1,	:nAdjParamVal2,	:nAdjParamVal3,'+
    '	:dwChgParamVal1,	:dwChgParamVal2,	:dwChgParamVal3,	:dwdestData1,	:dwdestData2,	:dwdestData3,	:dwactiveskill,'+
    '	:dwactiveskillLv,	:dwactiveskillper,	:dwReqMp,	:dwRepFp,	:dwReqDisLV,	:dwReSkill1,	:dwReSkillLevel1,	:dwReSkill2,'+
    '	:dwReSkillLevel2,	:dwSkillReadyType,	:dwSkillReady,	:dwSkillRange,	:dwSfxElemental,	:dwSfxObj,	:dwSfxObj2,	:dwSfxObj3,'+
    '	:dwSfxObj4,	:dwSfxObj5,	:dwUseMotion,	:dwCircleTime,	:dwSkillTime,	:dwExeTarget,	:dwUseChance,	:dwSpellRegion,'+
    '	:dwSpellType,	:dwReferStat1,	:dwReferStat2,	:dwReferTarget1,	:dwReferTarget2,	:dwReferValue1,	:dwReferValue2,	:dwSkillType,'+
    '	:fItemResistElecricity,	:fItemResistFire,	:fItemResistWind,	:fItemResistWater,	:fItemResistEarth,	:nEvildoing,	:dwExpertLV,	:ExpertMax,'+
    '	:dwSubDefine,	:dwExp,	:dwComboStyle,	:fFlightSpeed,	:fFlightLRAngle,	:fFlightTBAngle,	:dwFlightLimit,	:dwFFuelReMax,'+
    '	:dwAFuelReMax,	:dwFuelRe,	:dwLimitLevel1,	:dwReflect,	:dwSndAttack1,	:dwSndAttack2,	:szIcon,	:dwQuestID,'+
    '	:szTextFile,	:szComment)');

    DM.qMasterDBTransfer.ParamByName('ver6').AsString                        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwID').AsString                        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('szName').AsString                      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwNum').AsString                       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwPackMax').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemKind1').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemKind2').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemKind3').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemJob').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bPermanence').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwUseable').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemSex').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwCost').AsString                      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwEndurance').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nAbrasion').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nMaxRepair').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwHanded').AsString                    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwFlag').AsString                      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwParts').AsString                     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwPartsub').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bPartFile').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwExclusive').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwBasePartsIgnore').AsString           :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemLV').AsString                    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemRare').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwShopAble').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bLog').AsString                        :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bCharged').AsString                    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwLinkKindBullet').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwLinkKind').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAbilityMin').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAbilityMax').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('eItemType').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('wItemEAtk').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwParry').AsString                     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwBlockRating').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAddSkillMin').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAddSkillMax').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAtkStyle').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwWeaponType').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemAtkOrder1').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemAtkOrder2').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemAtkOrder3').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwItemAtkOrder4').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('bContinuousPain').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwShellQuantity').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwRecoil').AsString                    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwLoadingTime').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nAdjHitRate').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAttackSpeed').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwDmgShift').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAttackRange').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwProbability').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwDestParam1').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwDestParam2').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwDestParam3').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nAdjParamVal1').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nAdjParamVal2').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nAdjParamVal3').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwChgParamVal1').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwChgParamVal2').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwChgParamVal3').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwdestData1').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwdestData2').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwdestData3').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwactiveskill').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwactiveskillLv').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwactiveskillper').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReqMp').AsString                     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwRepFp').AsString                     :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReqDisLV').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReSkill1').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReSkillLevel1').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReSkill2').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReSkillLevel2').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSkillReadyType').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSkillReady').AsString                :=    GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSkillRange').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSfxElemental').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSfxObj').AsString                    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSfxObj2').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSfxObj3').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSfxObj4').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSfxObj5').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwUseMotion').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwCircleTime').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSkillTime').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwExeTarget').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwUseChance').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSpellRegion').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSpellType').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReferStat1').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReferStat2').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReferTarget1').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReferTarget2').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReferValue1').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReferValue2').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSkillType').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fItemResistElecricity').AsString       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fItemResistFire').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fItemResistWind').AsString             :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fItemResistWater').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fItemResistEarth').AsString            :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('nEvildoing').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwExpertLV').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('ExpertMax').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSubDefine').AsString                 :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwExp').AsString                       :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwComboStyle').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fFlightSpeed').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fFlightLRAngle').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('fFlightTBAngle').AsString              :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwFlightLimit').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwFFuelReMax').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwAFuelReMax').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwFuelRe').AsString                    :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwLimitLevel1').AsString               :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwReflect').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndAttack1').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwSndAttack2').AsString                :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('szIcon').AsString                      :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('dwQuestID').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('szTextFile').AsString                  :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ParamByName('szComment').AsString                   :=   GetToken(ParamStr, '|', '|');
    DM.qMasterDBTransfer.ExecSQL;
  end
  else
  begin
     { DM.qMasterDBTransfer.SQL.Text := '';
    DM.qMasterDBTransfer.SQL.Text := 'UPDATE FRE_Monster SET Count = Count + 1 WHERE Word = :word';
    DM.qMasterDBTransfer.ParamByName('word').AsString := tmpWord;
    DM.qMasterDBTransfer.ExecSQL;     }
  end;
  Result := True;
end;

function TMainForm.ParsePropMoverEx(Filepath: String): String;
var
  TargetList: TStringList;
  sOriginalStr: String;
begin
  TargetList := TStringList.Create;
  TargetList.LoadFromFile(Filepath);
  sOriginalStr        := TargetList.Text;
  Result              := sOriginalStr;

  propMoverEx.Assign(TargetList);
  //cxMemo1.Lines := TargetList;
  TargetList.Free;
end;

function TMainForm.GetBetween (Str: String; StrStart : String; StrEnd : String) : String;
var
  iPos: Integer;
begin
  result := '';
  iPos := Pos (StrStart, Str);
  if iPos <> 0 then begin
    Delete (Str, 1, iPos + Length (StrStart) - 1);
    iPos := Pos (StrEnd, Str);
    if iPos <> 0 then begin
      result := Copy(Str,1, iPos - 1);
    end;
  end;
end;

function TMainForm.GetMIContainerSegment(MIContainerStr, TargetMISegment: String): String;   // MI = Monster Container in der propMoverEx.inc
var
 TargetMI, NeighbourMI, TargetMILength: Integer;
 MIContainer, NeighbourMIStr: String;
 bKlammerAuf, bKlammerZu: Boolean;
 NeighbourMIComma, NeighbourMIStrLength: Integer;
                                                                      // MI_AIBATT3 { .. }
begin
//  MIContainerStr := StringReplace(MIContainerStr,#$A,'',[rfReplaceAll]); //remove ASCII line feed and carriage return characters
//  MIContainerStr := StringReplace(MIContainerStr,#$D,'',[rfReplaceAll]);  //remove ASCII line feed and carriage return characters

  TargetMI := Pos(TargetMISegment, MIContainerStr);
  TargetMILength := Length(TargetMISegment);


  //Linefeed OFF

  NeighbourMI    := Pos('MI_', MIContainerStr, TargetMI + TargetMILength);
  NeighbourMIComma := Pos(',', MIContainerStr, NeighbourMI);
  NeighbourMIStr := Copy(MIContainerStr, NeighbourMI - 1, NeighbourMIComma - NeighbourMI + 1);
  NeighbourMIStrLength := Length(NeighbourMIStr);

  bKlammerZu  := IsSubStringAt('}', NeighbourMIStr, 1);
  bKlammerAuf := IsSubStringAt('{', NeighbourMIStr, NeighbourMIStrLength);


  if (bKlammerZu = True) and (bKlammerAuf = True)  then // Wenn er das richtige MI Segment gefunden hat
  begin
    MIContainer := GetBetween(MIContainerStr, TargetMISegment, 'MI_');
  end else //Wenn das MI_MONSTER keine } MI_ { davor hat, das n�chste MI per Pos suchen von NeighbourMI + NeighbourMIStrLength
  begin
    MIContainer := GetBetween(MIContainerStr, TargetMISegment, '}'#$D#$A'MI_');
  end;

  //Linefeed ON
  MonsterPropertyAdvanced.MIContainer := MIContainer; //Aktuelle geparstes MIContainer Element
  Result:= MIContainer;
end;

function TMainForm.IsSubStringAt(const needle, haystack: string; position: Integer): Boolean;
var
  substr: string;
begin
  substr := Copy(haystack, position, Length(needle));
  Result := substr = needle;
end;

function TMainForm.GetToken(var S: string; const Separators: string; const Stop: string = string.Empty): string;
var
  I: Integer;
  Breaks: string;
begin
  Breaks := Separators + Stop;
  for I := 0 to S.Length - 1 do
    if Breaks.Contains(S.Chars[I]) then
      Break;

  Result := S.Substring(0, I);
  S := S.Substring(I + 1);
end;

procedure TMainForm.ClearTMonsterPropertyAdvancedRecord;
begin
    MonsterPropertyAdvanced.dwID := '';
    MonsterPropertyAdvanced.szName := '';
    MonsterPropertyAdvanced.dwAI := '';
    MonsterPropertyAdvanced.dwIngameName := '';
    MonsterPropertyAdvanced.dwStr := '';
    MonsterPropertyAdvanced.dwSta := '';
    MonsterPropertyAdvanced.dwDex := '';
    MonsterPropertyAdvanced.dwInt := '';
    MonsterPropertyAdvanced.dwLevel := '';
    MonsterPropertyAdvanced.dwClass := '';
    MonsterPropertyAdvanced.dwAtkMin := '';
    MonsterPropertyAdvanced.dwAtkMax := '';
    MonsterPropertyAdvanced.dwAttackSpeed := '';
    MonsterPropertyAdvanced.dwReAttackDelay := '';
    MonsterPropertyAdvanced.dwAddHp := '';
    MonsterPropertyAdvanced.dwAddMp := '';
    MonsterPropertyAdvanced.dwNaturealArmor := '';
    MonsterPropertyAdvanced.eElementType := '';
    MonsterPropertyAdvanced.dwCash := '';
    MonsterPropertyAdvanced.dwExpValue := '';
    MonsterPropertyAdvanced.bKillable := '';
end;

procedure TMainForm.FillAdvancedGroupbox;
begin
  cxTextEditID.Text          := MonsterPropertyAdvanced.dwID;
  cxTextEditName.Text        := MonsterPropertyAdvanced.szName;
  cxTextEditAI.Text          := MonsterPropertyAdvanced.dwAI;
  cxTextEditIngameName.Text  := MonsterPropertyAdvanced.dwIngameName;
  cxTextEditStr.Text         := MonsterPropertyAdvanced.dwStr;
  cxTextEditSta.Text         := MonsterPropertyAdvanced.dwSta;
  cxTextEditDex.Text         := MonsterPropertyAdvanced.dwDex;
  cxTextEditInt.Text         := MonsterPropertyAdvanced.dwInt;
  cxTextEditLevel.Text       := MonsterPropertyAdvanced.dwLevel;
  cxTextEditRank.Text        := MonsterPropertyAdvanced.dwClass;
  cxTextEditMinAtk.Text      := MonsterPropertyAdvanced.dwAtkMin;
  cxTextEditMaxAtk.Text      := MonsterPropertyAdvanced.dwAtkMax;
  cxTextEditAtkSpeed.Text    := MonsterPropertyAdvanced.dwAttackSpeed;
  cxTextEditAtkDelay.Text    := MonsterPropertyAdvanced.dwReAttackDelay;
  cxTextEditHP.Text          := MonsterPropertyAdvanced.dwAddHp;
  cxTextEditMP.Text          := MonsterPropertyAdvanced.dwAddMp;
  cxTextEditDefense.Text     := MonsterPropertyAdvanced.dwNaturealArmor;
  cxTextEditElement.Text     := MonsterPropertyAdvanced.eElementType;
  cxTextEditExp.Text         := MonsterPropertyAdvanced.dwExpValue;
  cxCheckBoxKillable.Checked := StrToBool(MonsterPropertyAdvanced.bKillable);
end;


procedure TMainForm.UnlockAdvancedGroupbox;
begin
  cxTextEditID.Properties.ReadOnly := False;
  cxTextEditName.Properties.ReadOnly := False;
  cxTextEditAI.Properties.ReadOnly := False;
  cxTextEditIngameName.Properties.ReadOnly := False;
  cxTextEditStr.Properties.ReadOnly := False;
  cxTextEditSta.Properties.ReadOnly := False;
  cxTextEditDex.Properties.ReadOnly := False;
  cxTextEditInt.Properties.ReadOnly := False;
  cxTextEditLevel.Properties.ReadOnly := False;
  cxTextEditRank.Properties.ReadOnly := False;
  cxTextEditMinAtk.Properties.ReadOnly := False;
  cxTextEditMaxAtk.Properties.ReadOnly := False;
  cxTextEditAtkSpeed.Properties.ReadOnly := False;
  cxTextEditAtkDelay.Properties.ReadOnly := False;
  cxTextEditHP.Properties.ReadOnly := False;
  cxTextEditMP.Properties.ReadOnly := False;
  cxTextEditDefense.Properties.ReadOnly := False;
  cxTextEditElement.Properties.ReadOnly := False;
  cxTextEditExp.Properties.ReadOnly := False;
  cxCheckBoxKillable.Properties.ReadOnly := False;
  cxSaveAdvanceButton.Enabled := True;

  cxTextEditID.Enabled := True;
  cxTextEditName.Enabled := True;
  cxTextEditAI.Enabled := True;
  cxTextEditIngameName.Enabled := True;
  cxTextEditStr.Enabled := True;
  cxTextEditSta.Enabled := True;
  cxTextEditDex.Enabled := True;
  cxTextEditInt.Enabled := True;
  cxTextEditLevel.Enabled := True;
  cxTextEditRank.Enabled := True;
  cxTextEditMinAtk.Enabled := True;
  cxTextEditMaxAtk.Enabled := True;
  cxTextEditAtkSpeed.Enabled := True;
  cxTextEditAtkDelay.Enabled := True;
  cxTextEditHP.Enabled := True;
  cxTextEditMP.Enabled := True;
  cxTextEditDefense.Enabled := True;
  cxTextEditElement.Enabled := True;
  cxTextEditExp.Enabled := True;
  cxCheckBoxKillable.Enabled := True;
  cxSaveAdvanceButton.Enabled := True;

  bAdvancedIsLocked := False;
end;

procedure TMainForm.LockAdvancedGroupbox;
begin
  cxTextEditID.Properties.ReadOnly := True;
  cxTextEditName.Properties.ReadOnly := True;
  cxTextEditAI.Properties.ReadOnly := True;
  cxTextEditIngameName.Properties.ReadOnly := True;
  cxTextEditStr.Properties.ReadOnly := True;
  cxTextEditSta.Properties.ReadOnly := True;
  cxTextEditDex.Properties.ReadOnly := True;
  cxTextEditInt.Properties.ReadOnly := True;
  cxTextEditLevel.Properties.ReadOnly := True;
  cxTextEditRank.Properties.ReadOnly := True;
  cxTextEditMinAtk.Properties.ReadOnly := True;
  cxTextEditMaxAtk.Properties.ReadOnly := True;
  cxTextEditAtkSpeed.Properties.ReadOnly := True;
  cxTextEditAtkDelay.Properties.ReadOnly := True;
  cxTextEditHP.Properties.ReadOnly := True;
  cxTextEditMP.Properties.ReadOnly := True;
  cxTextEditDefense.Properties.ReadOnly := True;
  cxTextEditElement.Properties.ReadOnly := True;
  cxTextEditExp.Properties.ReadOnly := True;
  cxCheckBoxKillable.Properties.ReadOnly := True;
  cxSaveAdvanceButton.Enabled := False;

  cxTextEditID.Enabled := False;
  cxTextEditName.Enabled := False;
  cxTextEditAI.Enabled := False;
  cxTextEditIngameName.Enabled := False;
  cxTextEditStr.Enabled := False;
  cxTextEditSta.Enabled := False;
  cxTextEditDex.Enabled := False;
  cxTextEditInt.Enabled := False;
  cxTextEditLevel.Enabled := False;
  cxTextEditRank.Enabled := False;
  cxTextEditMinAtk.Enabled := False;
  cxTextEditMaxAtk.Enabled := False;
  cxTextEditAtkSpeed.Enabled := False;
  cxTextEditAtkDelay.Enabled := False;
  cxTextEditHP.Enabled := False;
  cxTextEditMP.Enabled := False;
  cxTextEditDefense.Enabled := False;
  cxTextEditElement.Enabled := False;
  cxTextEditExp.Enabled := False;
  cxCheckBoxKillable.Enabled := False;
  cxSaveAdvanceButton.Enabled := False;

  bAdvancedIsLocked := True;
end;



{
	DropKind(IK3_SWD, 1, 6);
	DropKind(IK3_HELMET, 1, 6);
	DropKind(IK3_SUIT, 1, 6);
	DropKind(IK3_GAUNTLET, 1, 6);
	DropKind(IK3_BOOTS, 1, 6);
	DropKind(IK3_SHIELD, 1, 6);
}

{DropKind( < IK3 >, < Item Unique Min. >, < Item Unique Max. > );}
function TMainForm.GetAllDropKind(MIContainerStr: String): Boolean;
var
  i, xPos: Integer;
  sTmp: String;
begin
  dropKindList.Text := MIContainerStr;

  for i := dropKindList.Count -1 downto 0 do
  begin
    sTmp    := ReplaceAllEvilChars(dropKindList[i]);
    xPos    := ansipos('DropKind', sTmp);

    if xPos = 0
      then begin
        dropKindList.Delete(i);
      end;
  end;

  if dropKindList.Count >= 1
    then begin
      Result := True;
    end else
      Result := False;
end;

procedure TMainForm.FillListItemDropGroupView;
var
  i: Integer;
  Item: TListItem;
  s: String;
begin
  ShowMessage(IntToStr(dropKindList.Count));

  for i := 0 to dropKindList.Count -1 do
    begin
      Item := cxListItemDropGroupView.Items.Add;
      s := dropKindList[i];
      Item.Caption        :=  'IK3_SUIT';
      Item.SubItems.Add('1');
      Item.SubItems.Add('6');
    end;
end;

function TMainForm.GetAllDropItem(MIContainerStr: String): Boolean;
var
  i, xPos: Integer;
  sTmp: String;
begin
  dropItemList.Text := MIContainerStr;

  for i := dropItemList.Count -1 downto 0 do
  begin
    sTmp    := ReplaceAllEvilChars(dropItemList[i]);
    xPos    := AnsiPos('DropItem', sTmp);

    if xPos = 0
      then begin
        dropItemList.Delete(i);
      end;
  end;

  if dropItemList.Count >= 1
    then begin
      Result := True;
    end else
      Result := False;
end;

function TMainForm.GetItemFromMasterDB: Integer;
var
  Itemname: String;
begin
  DM.qItem.SQL.Text := '';
  DM.qItem.SQL.Text := 'SELECT * FROM FRE_Item';
  DM.qItem.Open;

  try
    cxListBoxItem.Items.Clear;

    while not DM.qItem.Eof do
    begin
      Itemname := DM.qItem.Fields[2].AsString;
      cxListBoxItem.Items.Add(Itemname);
      DM.qItem.Next;
    end;

  finally
    Result := DM.qItem.RecordCount;
    DM.qItem.Close;
  end;
end;

end.
